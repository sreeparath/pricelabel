package com.dcode.pricelabel.data;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.dcode.pricelabel.data.model.BFO_ITEM;
import com.dcode.pricelabel.data.model.BFO_PRICE_CHECK;
import com.dcode.pricelabel.data.model.BINS;
import com.dcode.pricelabel.data.model.BIN_ITEMS;
import com.dcode.pricelabel.data.model.ITEMS;

@Database(entities = {
        BINS.class,
        BIN_ITEMS.class,
        ITEMS.class,
        BFO_ITEM.class,
        BFO_PRICE_CHECK.class,
}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {
    private static AppDatabase mInstance;

    static synchronized AppDatabase getInstance(Context context) {
        if (mInstance == null) {

            mInstance = Room.databaseBuilder(context, AppDatabase.class, "Data.db")
                    .allowMainThreadQueries()
                    .addCallback(new RoomDatabase.Callback() {
                        @Override
                        public void onCreate(@NonNull SupportSQLiteDatabase db) {
                            super.onCreate(db);
                        }

                        @Override
                        public void onOpen(@NonNull SupportSQLiteDatabase db) {
//                            attachDatabase(AppVariables.getMastersDb());
                            super.onOpen(db);
                        }
                    })
                    .build();
        }
        return mInstance;
    }

//    private static void attachDatabase(final String mastersDb) {
//        String sql = String.format("ATTACH DATABASE '%s' AS \"%s\";", mastersDb, "Items");
//        mInstance.mDatabase.execSQL(sql);
//    }

    public abstract GenericDao genericDao();
}
