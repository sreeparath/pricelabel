package com.dcode.pricelabel.data;

import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.dcode.pricelabel.App;
import com.dcode.pricelabel.data.model.BFO_ITEM;
import com.dcode.pricelabel.data.model.BFO_PRICE_CHECK;
import com.dcode.pricelabel.data.model.ITEMS;
import com.dcode.pricelabel.data.model.STORES;
import com.dcode.pricelabel.data.model.USERS;

import java.util.ArrayList;
import java.util.List;

public class MastersDBManager {

    private SQLiteDatabase mDatabase;



    private String db_file;

    public MastersDBManager(String masters_db) {
        db_file = masters_db;
    }

    public void open() throws SQLException {
        if (mDatabase == null || !mDatabase.isOpen()) {
            mDatabase = SQLiteDatabase.openDatabase(db_file, null, SQLiteDatabase.OPEN_READONLY);
        }
    }

    public void close() {
        if (mDatabase != null && mDatabase.isOpen()) {
            mDatabase.close();
            mDatabase = null;
        }
    }

    public BFO_ITEM getItemsByBarcodeBFO(String barcode) {
        String sqlTemplate = "select * from ITEMS where BARCODE = '%s'";
        String staractquery = String.format(sqlTemplate, barcode);
        Cursor cursor = mDatabase.rawQuery(staractquery, null);

        BFO_ITEM items = null;

        if (cursor != null && cursor.getCount() > 0) {
            items = new BFO_ITEM();
            cursor.moveToFirst();
            items.ITEM_NO = cursor.getString(cursor.getColumnIndex("ITEM_NO"));
            items.BARCODE = cursor.getString(cursor.getColumnIndex("BARCODE"));
            items.ITEM_DESCRIPTION = cursor.getString(cursor.getColumnIndex("LONGDESC"));
            items.SIZE_DESCR = cursor.getString(cursor.getColumnIndex("SIZE"));
            items.PRICE = Double.parseDouble(cursor.getString(cursor.getColumnIndex("RRP_RTL")));
            items.QUANTITY = 1;
//            items.SCALE = cursor.getString(cursor.getColumnIndex("SIZE_SCALE"));
//            items.COLOUR = cursor.getString(cursor.getColumnIndex("COLOUR"));
//            items.BRAND = cursor.getString(cursor.getColumnIndex("BRAND"));
//            items.CATEGOTY = cursor.getString(cursor.getColumnIndex("ITCAT"));
//            items.UOM = cursor.getString(cursor.getColumnIndex("BUNIT"));
            cursor.close();
        }
        return items;
    }

    public ITEMS getItemsByBarcode(String barcode) {
        String sqlTemplate = "select * from ITEMS where BARCODE = '%s'";
        String staractquery = String.format(sqlTemplate, barcode);
        Cursor cursor = mDatabase.rawQuery(staractquery, null);

        ITEMS items = null;

        if (cursor != null && cursor.getCount() > 0) {
            items = new ITEMS();
            cursor.moveToFirst();
            items.ITEM_CODE = cursor.getString(cursor.getColumnIndex("ITEM_CODE"));
            items.BARCODE = cursor.getString(cursor.getColumnIndex("BARCODE"));
            items.SIZE = cursor.getString(cursor.getColumnIndex("SIZE"));
            items.LONGDESC = cursor.getString(cursor.getColumnIndex("LONGDESC"));
            items.OLD_PRICE = Float.parseFloat(cursor.getString(cursor.getColumnIndex("OLD_PRICE")));
            items.NEW_PRICE = Float.parseFloat(cursor.getString(cursor.getColumnIndex("NEW_PRICE")));
            items.LOCAL_DESCR = cursor.getString(cursor.getColumnIndex("LOCAL_DESCR"));
            items.COLOUR = cursor.getString(cursor.getColumnIndex("COLOUR"));
            items.BRAND = cursor.getString(cursor.getColumnIndex("BRAND"));
            cursor.close();
        }
        return items;
    }



    public List<STORES> getStores() {
        List<STORES> storesList = new ArrayList<>();
        try {
            String sqlQuery = "select * from STORES";
            Cursor cursor = mDatabase.rawQuery(sqlQuery, null);

            STORES stores;
            if (cursor != null && cursor.getCount() > 0) {
                for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                    stores = new STORES();
                    stores.STORE_CODE = cursor.getString(cursor.getColumnIndex("STORE_CODE"));
                    stores.STORE_NAME = cursor.getString(cursor.getColumnIndex("STORE_NAME"));
                    stores.VALIDATION_TYPE = cursor.getString(cursor.getColumnIndex("VALIDATION_TYPE"));
                    storesList.add(stores);
                }
                cursor.close();
            }
        } catch (Exception e) {
            Log.d(App.TAG, e.toString());
        }
        return storesList;
    }

    public STORES getStore(String code) {
        STORES stores = null;
        try {
            String sqlQuery = String.format("select * from STORES WHERE STORE_CODE = '%s'", code);
            Cursor cursor = mDatabase.rawQuery(sqlQuery, null);

            if (cursor != null && cursor.getCount() > 0) {
                cursor.moveToFirst();
                stores = new STORES();
                stores.STORE_CODE = cursor.getString(cursor.getColumnIndex("STORE_CODE"));
                stores.STORE_NAME = cursor.getString(cursor.getColumnIndex("STORE_NAME"));
                stores.VALIDATION_TYPE = cursor.getString(cursor.getColumnIndex("VALIDATION_TYPE"));
                cursor.close();
            }
        } catch (Exception e) {
            Log.d(App.TAG, e.toString());
        }
        return stores;
    }

    public USERS getUser(String login_name, String passwd) {
        String sqlTemplate = "SELECT * FROM USERS WHERE LOGIN_NAME = '%s' AND PASSWD = '%s'";
        Cursor cursor = mDatabase.rawQuery(String.format(sqlTemplate, login_name, passwd), null);

        USERS users = null;
        if (cursor != null && cursor.getCount() > 0) {
            users = new USERS();
            cursor.moveToFirst();
            users.LOGIN_NAME = cursor.getString(cursor.getColumnIndex("LOGIN_NAME"));
            users.PASSWD = cursor.getString(cursor.getColumnIndex("PASSWD"));
            users.USER_GROUP = cursor.getString(cursor.getColumnIndex("USER_GROUP"));
            cursor.close();
        }
        return users;
    }
}