package com.dcode.pricelabel.data.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "ITEMS")
public class ITEMS {
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "BARCODE")
    public String BARCODE = "";

    @ColumnInfo(name = "ITEM_CODE")
    public String ITEM_CODE = "";

    @ColumnInfo(name = "SIZE")
    public String SIZE = "";

    @ColumnInfo(name = "COLOUR")
    public String COLOUR = "";

    @ColumnInfo(name = "BRAND")
    public String BRAND = "";

    @ColumnInfo(name = "LONGDESC")
    public String LONGDESC = "";

    @ColumnInfo(name = "OLD_PRICE")
    public float OLD_PRICE;

    @ColumnInfo(name = "NEW_PRICE")
    public float NEW_PRICE;

    @ColumnInfo(name = "LOCAL_DESCR")
    public String LOCAL_DESCR = "";
}
