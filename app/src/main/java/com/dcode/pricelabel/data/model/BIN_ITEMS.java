package com.dcode.pricelabel.data.model;

import android.annotation.SuppressLint;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "BIN_ITEMS")
public class BIN_ITEMS {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "PK_ID")
    public int PK_ID;

    @ColumnInfo(name = "ScanTime")
    public String ScanTime;

    /*case ItemVerification, QTY = 1: warn /0: no warn, else counted qty, should be >=1 */
    @ColumnInfo(name = "QTY")
    public long QTY;

    @ColumnInfo(name = "MATCH_FLAG")
    public String MATCH_FLAG;

    @ColumnInfo(name = "BIN_ID")
    public int BIN_ID;

    @ColumnInfo(name = "BARCODE")
    public String BARCODE = "";

    @ColumnInfo(name = "ITEM_NO")
    public String ITEM_NO = "";

    @ColumnInfo(name = "PRODUCT_CODE")
    public String PRODUCT_CODE = "";

    @ColumnInfo(name = "BRAND")
    public String BRAND = "";

    @ColumnInfo(name = "ARTICLE_NO")
    public String ARTICLE_NO = "";

    @ColumnInfo(name = "ITEM_DESCRIPTION")
    public String ITEM_DESCRIPTION = "";

    @ColumnInfo(name = "VARIANT_CODE")
    public String VARIANT_CODE = "";

    @ColumnInfo(name = "SIZE_DESCR")
    public String SIZE_DESCR = "";

    @ColumnInfo(name = "QUANTITY")
    public String QUANTITY = "";

    @ColumnInfo(name = "ALT_ITM")
    public String ALT_ITM = "";

    @ColumnInfo(name = "CUSTOM_1")
    public String CUSTOM_1 = "";

    @ColumnInfo(name = "CUSTOM_2")
    public String CUSTOM_2 = "";

    @SuppressLint("DefaultLocale")
    @NonNull
    @Override
    public String toString() {
        return String.format("%s, %s, %s, %s, %s, %s,%s, %d, %s, %s",
                BARCODE, ITEM_NO, PRODUCT_CODE, BRAND, ARTICLE_NO, ITEM_DESCRIPTION, ALT_ITM, QTY, MATCH_FLAG, ScanTime);
    }

    @SuppressLint("DefaultLocale")
    @NonNull
    public String toString(String destLoc) {
        return String.format("%s, %s, %s, %s, %s, %s, %s,%s, %d, %s, %s",
                destLoc, BARCODE, ITEM_NO, PRODUCT_CODE, BRAND, ARTICLE_NO, ITEM_DESCRIPTION, ALT_ITM, QTY, MATCH_FLAG, ScanTime);
    }

    @SuppressLint("DefaultLocale")
    @NonNull
    public String GetBinBarcodeString(String bin) {
        return String.format("%s, %s",
                bin, BARCODE);
    }
}