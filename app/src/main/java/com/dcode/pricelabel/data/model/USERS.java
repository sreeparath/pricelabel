package com.dcode.pricelabel.data.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import org.jetbrains.annotations.NotNull;

@Entity(tableName = "USERS")
public class USERS {
    @PrimaryKey
    @NotNull
    @ColumnInfo(name = "LOGIN_NAME")
    public String LOGIN_NAME;

    @ColumnInfo(name = "PASSWD")
    public String PASSWD;

    @ColumnInfo(name = "USER_GROUP")
    public String USER_GROUP;
}
