package com.dcode.pricelabel.data.model;

public class BINS_SUMMARY {

    public int PK_ID;

    public String BIN_NO;

    public int BIN_MODE;

    public int BIN_PRESET = -1;

    public String USER_NAME;

    public String MODE;

    public int ITEM_COUNT;
}
