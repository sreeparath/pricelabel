package com.dcode.pricelabel.data.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "BFO_PRICE_CHECK")
public class BFO_PRICE_CHECK {

    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "SL_NO")
    public long SL_NO ;

    @NonNull
    @ColumnInfo(name = "BARCODE")
    public String BARCODE = "";

    @NonNull
    @ColumnInfo(name = "ITEM_NO")
    public String ITEM_NO = "";

    @ColumnInfo(name = "ITEM_DESCRIPTION")
    public String ITEM_DESCRIPTION = "";

    @ColumnInfo(name = "SIZE_DESCR")
    public String SIZE_DESCR = "";

    @ColumnInfo(name = "QUANTITY")
    public int QUANTITY = 0;

    @ColumnInfo(name = "PRICE")
    public double PRICE = 0;

    @ColumnInfo(name = "IS_VALIDATE")
    public boolean IS_VALIDATE = false;

    @ColumnInfo(name = "SCALE")
    public String SCALE = "";

    @ColumnInfo(name = "UOM")
    public String UOM = "";

    @ColumnInfo(name = "BRAND")
    public String BRAND = "";

    @ColumnInfo(name = "CATEGOTY")
    public String CATEGOTY = "";

    @ColumnInfo(name = "COLOUR")
    public String COLOUR = "";

    @ColumnInfo(name = "SCAN_TIME")
    public String SCAN_TIME = "";
}
