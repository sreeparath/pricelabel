package com.dcode.pricelabel.data.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "BINS")
public class BINS implements Serializable {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "PK_ID")
    public int PK_ID;

    @ColumnInfo(name = "BIN_NO")
    public String BIN_NO;

    /*1: StockTake, 2: Transfer, 3: ItemVerification*/
    @ColumnInfo(name = "BIN_MODE")
    public int BIN_MODE;

    @ColumnInfo(name = "STORE_CODE")
    public String STORE_CODE;

    @ColumnInfo(name = "DEST_LOC", defaultValue = "")
    public String DEST_LOC;

    @ColumnInfo(name = "USER_NAME")
    public String USER_NAME;
}
