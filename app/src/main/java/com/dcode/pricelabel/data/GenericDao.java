package com.dcode.pricelabel.data;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.RawQuery;
import androidx.room.SkipQueryVerification;
import androidx.sqlite.db.SupportSQLiteQuery;

import com.dcode.pricelabel.data.model.BFO_ITEM;
import com.dcode.pricelabel.data.model.BFO_PRICE_CHECK;
import com.dcode.pricelabel.data.model.BINS;
import com.dcode.pricelabel.data.model.BIN_ITEMS;
import com.dcode.pricelabel.data.model.GENERIC_COUNT;
import com.dcode.pricelabel.data.model.ITEMS;
import com.dcode.pricelabel.data.model.MAX_VALUE;

import java.util.List;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface GenericDao {

    @RawQuery
    MAX_VALUE getMaxValue(SupportSQLiteQuery query);

    @RawQuery
    GENERIC_COUNT getCount(SupportSQLiteQuery query);

//    @SkipQueryVerification
//    @Query("SELECT * FROM STORES")
//    List<STORES> getAllStores();
//
//    @SkipQueryVerification
//    @Query("SELECT * FROM USERS WHERE LOGIN_NAME = :login_name AND PASSWD = :passwd")
//    USERS getUser(String login_name, String passwd);

    @SkipQueryVerification
    @Query("SELECT * FROM ITEMS WHERE BARCODE=:barcode")
    ITEMS getItemsByBarcode(String barcode);

    @Insert(onConflict = REPLACE)
    void insertItem(ITEMS... items);

    @Query("DELETE FROM main.BINS")
    void deleteAllBins();

    @Query("DELETE FROM main.BINS WHERE PK_ID=:pk_id")
    void deleteBin(int pk_id);

    @Insert(onConflict = REPLACE)
    void insertBin(BINS... bins);

    @Query("SELECT * FROM BINS WHERE PK_ID = :pk_id")
    BINS getBinByID(int pk_id);

    @Query("SELECT * FROM main.BINS WHERE BIN_NO=:bin_no AND BIN_MODE = 1 AND USER_NAME=:user_name")
    BINS getBinByBinNoMode1(String bin_no, String user_name);

    @Query("SELECT * FROM main.BINS WHERE BIN_NO=:bin_no AND BIN_MODE = 2 AND DEST_LOC = :destLoc AND USER_NAME=:user_name")
    BINS getBinByBinNoMode2(String bin_no, String destLoc, String user_name);

    @Query("DELETE FROM main.BIN_ITEMS WHERE BIN_ID=:pk_id")
    void deleteAllByBin(int pk_id);

    @Insert(onConflict = REPLACE)
    void insertBin_Items(BIN_ITEMS... bin_items);

    @Query("DELETE FROM BIN_ITEMS WHERE PK_ID=:pk_id")
    void deleteBinItemByID(long pk_id);

    @Query("SELECT * " +
            "FROM BIN_ITEMS " +
            "WHERE BIN_ID=:bin_id ")
    List<BIN_ITEMS> getItemsByBin(int bin_id);

    @Insert(onConflict = REPLACE)
    void insertBFoItem(BFO_ITEM... bfo_items);

    @Query("SELECT * FROM BFO_ITEM")
    List<BFO_ITEM> getStockTakeItems();

    @Query("DELETE FROM BFO_ITEM WHERE SL_NO=:sl_no")
    void deleteStockItemBFo(long sl_no);

    @Query("DELETE FROM BFO_ITEM ")
    void deleteAllStockItemBFo();

    @Insert(onConflict = REPLACE)
    void insertBFoPriceCheck(BFO_PRICE_CHECK... bfo_price_checks);

    @Query("SELECT * FROM BFO_PRICE_CHECK")
    List<BFO_PRICE_CHECK> getPriceCheckItems();

    @Query("DELETE FROM BFO_PRICE_CHECK WHERE SL_NO=:sl_no")
    void deletePriceCheckItems(long sl_no);

    @Query("SELECT * FROM BFO_PRICE_CHECK")
    List<BFO_ITEM> getPriceCheckItemExport();

    @Query("DELETE FROM BFO_PRICE_CHECK ")
    void deleteAllBFoPriceCheck();

    @Query("SELECT 0 SL_NO,BARCODE,ITEM_DESCRIPTION,SUM(QUANTITY) AS QUANTITY,NULL AS ITEM_NO,PRICE,NULL AS IS_VALIDATE,SIZE_DESCR  FROM BFO_ITEM GROUP BY BARCODE,ITEM_DESCRIPTION,SIZE_DESCR,PRICE")
    List<BFO_ITEM> getStockSummary();
}
