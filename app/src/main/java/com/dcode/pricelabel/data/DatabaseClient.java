package com.dcode.pricelabel.data;

import android.content.Context;
import android.util.Log;

import com.dcode.pricelabel.App;

public class DatabaseClient {

    private static AppDatabase appDatabase;
    private static DatabaseClient mInstance;
/*    private String dbPath = "";

    private DatabaseClient(Context context) {

        dbPath = String.format("%s/data", App.getBaseDataPath());

        String dataFile = "";
        String masterFile = "";
        File outputFile = new File(dbPath);
        if (outputFile.mkdirs() || outputFile.isDirectory()) {
            dataFile = dbPath + "/Data.db";
        }

        appDatabase = Room
                .databaseBuilder(context, AppDatabase.class, dataFile)
                .allowMainThreadQueries()
                .addCallback(new RoomDatabase.Callback() {
                    @Override
                    public void onCreate(@NonNull SupportSQLiteDatabase db) {
                        super.onCreate(db);
                    }

                    @Override
                    public void onOpen(@NonNull SupportSQLiteDatabase db) {
                        attachDatabase(dbPath);
                        super.onOpen(db);
                    }
                })
                .build();
    }*/

    private DatabaseClient(Context context) {
        appDatabase = AppDatabase.getInstance(context);
    }

    public static synchronized DatabaseClient getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new DatabaseClient(context);
        }
        return mInstance;
    }

    public AppDatabase getAppDatabase() {
        if (!appDatabase.isOpen()) {
            appDatabase.getOpenHelper().getWritableDatabase();
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                Log.d(App.TAG, e.getMessage());
            }
        }

        return appDatabase;
    }
}
