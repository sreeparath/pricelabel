package com.dcode.pricelabel.ui.dialog;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.dcode.pricelabel.R;

import java.util.Objects;


public class CustomProgressDialog extends ProgressDialog {
    private TextView tvTitle, tvMessage;
    private Context context;
    private String title, message;

    public CustomProgressDialog(Context context, boolean isCancelable) {
        super(context);
        this.context = context;
        setCancelable(isCancelable);
        setIndeterminate(true);
        setCanceledOnTouchOutside(isCancelable);
    }

    public CustomProgressDialog(Context context, String title, String message, boolean isCancelable) {
        super(context);
        this.context = context;
        this.message = message;
        this.title = title;
        setCancelable(isCancelable);
        setIndeterminate(true);
        setCanceledOnTouchOutside(isCancelable);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.custom_progress_dialog);
        tvTitle = findViewById(R.id.tvTitle);
        tvMessage = findViewById(R.id.tvMessage);

        if (!TextUtils.isEmpty(message)) {
            tvMessage.setVisibility(View.VISIBLE);
            tvMessage.setText(message);
        }
        if (!TextUtils.isEmpty(title)) {
            tvTitle.setVisibility(View.VISIBLE);
            tvTitle.setText(title);
        }

        Objects.requireNonNull(getWindow()).setBackgroundDrawable(ContextCompat.getDrawable(context, R.color.color_transparent));
    }

    public void updateMessage(String message) {
        if (!TextUtils.isEmpty(message)) {
            tvMessage.setVisibility(View.VISIBLE);
            tvMessage.setText(message);
        }
    }

}
