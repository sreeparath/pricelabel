package com.dcode.pricelabel.ui.view;

import android.app.AlertDialog;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SearchView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.pricelabel.App;
import com.dcode.pricelabel.R;
import com.dcode.pricelabel.adapter.StockTakeItemAdapter;
import com.dcode.pricelabel.common.AppVariables;
import com.dcode.pricelabel.common.Utils;
import com.dcode.pricelabel.data.MastersDBManager;
import com.dcode.pricelabel.data.model.BFO_ITEM;
import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;



public class StockTakeFragmentNew
        extends BaseFragment
        implements View.OnClickListener,
        SearchView.OnQueryTextListener {

    BFO_ITEM bfo_item;
    List<BFO_ITEM> bfo_item_list;
    private TextInputEditText edScanCode;
    private TextInputEditText edItemCode;
    private TextInputEditText edItemDesc;
    private TextInputEditText edBarcode;
    private TextInputEditText edSize;
    private TextInputEditText edPrice;
    private TextInputEditText edReceiptQty;
    private TextView edTotlQty;
    private RadioGroup radioValidate;
    private RadioButton validateBtn;
    private View root;
    private StockTakeItemAdapter stockTakeitemAdapter;
    private AutoCompleteTextView tvUnits;
    private Drawable icon;
    private RecyclerView recyclerView;
    private final ColorDrawable background = new ColorDrawable(Color.RED);
    private float rcd_qty;
    private MastersDBManager itemsDB;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        if (this.getArguments() != null) {
            ModuleID = this.getArguments().getInt("ModuleID", 0);

            Bundle bundle = this.getArguments();
            if (bundle != null) {
                //pl_hdr = (LS_HDR) bundle.getSerializable(AppConstants.SELECTED_OBJECT);
            }
        }

        root = inflater.inflate(R.layout.fragment_stock_take_new, container, false);
        icon = getResources().getDrawable(R.drawable.ic_delete_black);
        setupView();
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        itemsDB = new MastersDBManager(AppVariables.getMastersDb());
        itemsDB.open();


        loadData();

        ItemTouchHelper.SimpleCallback simpleCallback =
                new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

                    @Override
                    public boolean onMove(@NonNull RecyclerView recyclerView,
                                          @NonNull RecyclerView.ViewHolder viewHolder,
                                          @NonNull RecyclerView.ViewHolder viewHolder1) {
                        return false;
                    }

                    @Override
                    public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                        if (direction == ItemTouchHelper.LEFT || direction == ItemTouchHelper.RIGHT) {
                            int position = viewHolder.getAdapterPosition();
                            BFO_ITEM pl_item_rec = stockTakeitemAdapter.getItemByPosition(position);
                            if (pl_item_rec != null) {
                                final AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
                                alert.setTitle("Delete this batch?");
                                alert.setPositiveButton("OK", (dialog, whichButton) -> {
                                    App.getDatabaseClient().getAppDatabase().genericDao().deleteStockItemBFo(pl_item_rec.SL_NO);
                                    recyclerView.removeViewAt(position);
                                    stockTakeitemAdapter.notifyItemRemoved(position);
                                    stockTakeitemAdapter.deleteBatch(position);
                                    stockTakeitemAdapter.notifyItemRangeChanged(position, stockTakeitemAdapter.getItemCount());
                                });
                                alert.setNegativeButton("Cancel", (dialog, whichButton) -> {
                                    //Put actions for CANCEL button here, or leave in blank
                                    stockTakeitemAdapter.notifyDataSetChanged();
                                });
                                alert.show();
                            }
                            UpdateBatchView();
                        }
                    }

                    @Override
                    public void onChildDraw(@NonNull Canvas c,
                                            @NonNull RecyclerView recyclerView,
                                            @NonNull RecyclerView.ViewHolder viewHolder,
                                            float dX, float dY, int actionState, boolean isCurrentlyActive) {
                        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
                        View itemView = viewHolder.itemView;
                        int backgroundCornerOffset = 20;

                        int iconMargin = (itemView.getHeight() - icon.getIntrinsicHeight()) / 2;
                        int iconTop = itemView.getTop() + (itemView.getHeight() - icon.getIntrinsicHeight()) / 2;
                        int iconBottom = iconTop + icon.getIntrinsicHeight();

                        if (dX > 0) {
                            int iconLeft = itemView.getLeft() + iconMargin + icon.getIntrinsicWidth();
                            int iconRight = itemView.getLeft() + iconMargin;
                            icon.setBounds(iconLeft, iconTop, iconRight, iconBottom);

                            background.setBounds(itemView.getLeft(), itemView.getTop(),
                                    itemView.getLeft() + ((int) dX) + backgroundCornerOffset, itemView.getBottom());
                        } else if (dX < 0) {
                            int iconLeft = itemView.getRight() - iconMargin - icon.getIntrinsicWidth();
                            int iconRight = itemView.getRight() - iconMargin;
                            icon.setBounds(iconLeft, iconTop, iconRight, iconBottom);

                            background.setBounds(itemView.getRight() + ((int) dX) - backgroundCornerOffset,
                                    itemView.getTop(), itemView.getRight(), itemView.getBottom());
                        } else {
                            background.setBounds(0, 0, 0, 0);
                        }
                        background.draw(c);
                        icon.draw(c);
                    }
                };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);
    }

    private void setupView() {

        radioValidate=root.findViewById(R.id.radioValidate);
        radioValidate.setOnCheckedChangeListener (new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int selectedId = radioValidate.getCheckedRadioButtonId();
                validateBtn = root.findViewById(selectedId);
                if (validateBtn.getText().toString().equals("Yes")) {
                    resetUI();
                } else if (validateBtn.getText().toString().equals("No")) {
                    resetUI();
                }
            }
        });

        edScanCode = root.findViewById(R.id.edScanCode);
        edScanCode.requestFocus();
        edScanCode.setOnKeyListener((v, keyCode, event) -> {
//            if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
//                    keyCode == KeyEvent.KEYCODE_ENTER) {
//                return onScanCodeKeyEvent();
//            }
            if(keyCode == KeyEvent.KEYCODE_ENTER){
                if(event.getAction() == KeyEvent.ACTION_DOWN){
                    return onScanCodeKeyEvent();
                }else{
                    return true;
                }
            }else{
                return true;
            }
            //return false;
        });

        edItemCode = root.findViewById(R.id.edItemcode);
        edItemDesc = root.findViewById(R.id.edItemDesc);
        edBarcode = root.findViewById(R.id.edBarCode);
        edTotlQty = root.findViewById(R.id.edTotlQty);

        //edSize = root.findViewById(R.id.edSize);
        //edPrice = root.findViewById(R.id.edPrice);
        //edReceiptQty = root.findViewById(R.id.edReceiptQty);
        //enableDisableUI(false);

        /*MaterialButton btnSave = root.findViewById(R.id.btnSave);
        btnSave.setOnClickListener(v -> onAddClick());
        MaterialButton btnClose = root.findViewById(R.id.btnClose);
        btnClose.setOnClickListener(v -> onNextClick());
        MaterialButton btnClear = root.findViewById(R.id.btnClear);
        btnClear.setOnClickListener(v -> onClearClick());*/

        recyclerView = root.findViewById(R.id.recyclerView);
        ((DefaultItemAnimator) Objects.requireNonNull(recyclerView.getItemAnimator())).setSupportsChangeAnimations(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        stockTakeitemAdapter = new StockTakeItemAdapter(new ArrayList<>(), this);
        recyclerView.setAdapter(stockTakeitemAdapter);

    }


    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        stockTakeitemAdapter.getFilter().filter(newText);
        String filterString = newText;
        return false;
    }

    @Override
    public void onClick(View view) {
//        LS_ITEMS ls_items = (LS_ITEMS) view.getTag();
//
//        if (ls_items == null) {
//            return;
//        }
    }


    private boolean onScanCodeKeyEvent() {
        String barcode = Objects.requireNonNull(edScanCode.getText()).toString().trim().replace("\r", "").replace("\n", "");
        BFO_ITEM items = null;
        if (barcode.isEmpty() || barcode.trim().length() == 0) {
            showToast("Invalid Barcode");
            return false;
        }

        int selectedId=radioValidate .getCheckedRadioButtonId();
        validateBtn =root.findViewById(selectedId);
        if(validateBtn.getText().equals("Yes")){
            items = itemsDB.getItemsByBarcodeBFO(barcode);
            if (items == null) {
                showAlert("Warning","Item Not Found!");
                //enableDisableUI(false);
                resetUI();
                edScanCode.requestFocus();
            }else{
                //enableDisableUI(false);
                displayItemData(items);
                SaveData(items);
                edScanCode.setText("");
                edScanCode.requestFocus();
            }
        }else if(validateBtn.getText().equals("No")){
            //enableDisableUI(true);
           // edReceiptQty.setEnabled(true);
            SaveData(items);
            resetUI();
            edScanCode.requestFocus();

        }
        return true;
    }

    private void enableDisableUI(boolean flag){
        edItemCode.setEnabled(flag);
        edItemDesc.setEnabled(flag);
        edSize.setEnabled(flag);
        edPrice.setEnabled(flag);
    }

    private void displayItemData(BFO_ITEM items) {
        edItemCode.setText(items.ITEM_NO);
        edItemDesc.setText(items.ITEM_DESCRIPTION);
        edBarcode.setText(items.BARCODE);
        //edSize.setText(items.SIZE_DESCR);
        //edReceiptQty.setText(String.valueOf(items.QUANTITY));
        //edPrice.setText(String.valueOf(items.PRICE));

    }

    private void onNextClick() {
        getActivity().onBackPressed();
    }

    private void onClearClick(){
        resetUI();
    }

//    private void onAddClick() {
//        if (IsDataValid()) {
//            SaveData();
//            edScanCode.requestFocus();
//        }
//    }

    private boolean IsDataValid() {
        /*String errMessage;

        String itcode = edScanCode.getText().toString().trim();
        if (itcode.length() == 0) {
            errMessage = "Invalid Item Code";
            showToast(errMessage);
            edItemCode.setError(errMessage);
            return false;
        }

        String itdesc = edItemCode.getText().toString().trim();
        if (itdesc.length() == 0) {
            errMessage = "Invalid Item Description";
            showToast(errMessage);
            edItemCode.setError(errMessage);
            return false;
        }

        String qty = edReceiptQty.getText().toString().trim();
        if (qty.length() == 0) {
            errMessage = "Receipt quantity not be zero";
            showToast(errMessage);
            edReceiptQty.setError(errMessage);
            return false;
        }*/

        return true;
    }

    private void SaveData(BFO_ITEM item) {
        if(item==null){
            item = new BFO_ITEM();
            item.BARCODE = edScanCode.getText().toString().trim();
        }
        //BFO_ITEM item = new BFO_ITEM();
        long slno = Utils.GetMaxValue("BFO_ITEM", "SL_NO", false, "", "");
        item.SL_NO = slno;
        item.SCAN_TIME = Utils.GetCurrentDateTime(Utils.DB_DATE_FORMAT);
        //item.BARCODE = edScanCode.getText().toString().trim();;
        //item.ITEM_NO = edItemCode.getText().toString().trim();
        //item.ITEM_DESCRIPTION = edItemDesc.getText().toString().trim();
        //item.SIZE_DESCR = edSize.getText().toString().trim();
        //item.QUANTITY = Integer.parseInt(edReceiptQty.getText().toString().trim());
        item.QUANTITY = 1;
        //item.PRICE = Double.parseDouble(edPrice.getText().toString().trim());
        int selectedId=radioValidate .getCheckedRadioButtonId();
        validateBtn =root.findViewById(selectedId);
        if(validateBtn.getText().equals("Yes")){
            item.IS_VALIDATE = Boolean.TRUE;
        }else if(validateBtn.getText().equals("No")){
            item.IS_VALIDATE = Boolean.FALSE;
        }

        App.getDatabaseClient().getAppDatabase().genericDao().insertBFoItem(item);
        loadData();
        //resetUI();
        //edScanCode.requestFocus();
    }

    private void resetUI(){
        edItemCode.setText("");
        edItemDesc.setText("");
        edBarcode.setText("");
        edScanCode.setText("");
        edScanCode.requestFocus();
    }

    private void loadData() {
        bfo_item_list = App.getDatabaseClient().getAppDatabase().genericDao().getStockTakeItems();
        if (bfo_item_list != null ) {
            stockTakeitemAdapter.addItems(bfo_item_list);
        }
        edTotlQty.setText("Total Scan Qty  : "+stockTakeitemAdapter.getItemCount());
    }

    private void UpdateBatchView() {
//        picklist_item_recList = App.getDatabaseClient().getAppDatabase().genericDao().GetLOAD_SLIP_ITEMS_RECEIPT(pl_hdr.LSNO);
//        if (picklist_item_recList != null ) {
//            plItemsRecAdapter.addItems(picklist_item_recList);
//        }
    }
}
