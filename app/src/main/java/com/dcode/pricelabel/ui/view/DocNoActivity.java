package com.dcode.pricelabel.ui.view;

import android.app.AlertDialog;
import android.content.Intent;
import android.database.SQLException;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

import com.dcode.pricelabel.App;
import com.dcode.pricelabel.R;
import com.dcode.pricelabel.common.AppConstants;
import com.dcode.pricelabel.common.AppVariables;
import com.dcode.pricelabel.common.CustomResult;
import com.dcode.pricelabel.common.Utils;
import com.dcode.pricelabel.data.MastersDBManager;
import com.dcode.pricelabel.data.model.BINS;
import com.dcode.pricelabel.data.model.STORES;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class DocNoActivity extends BaseActivity {
    private TextInputEditText edDocNo;
    private TextInputEditText edDestLoc;
    private AutoCompleteTextView tvWorkSites;
    private ArrayAdapter<STORES> storesArrayAdapter;
    private int binMode;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_doc_no;
    }

    @Override
    protected void onViewReady(Bundle savedInstanceState) {
        binMode = 2;

        edDocNo = findViewById(R.id.edDocNo);

        tvWorkSites = findViewById(R.id.acWorkSites);
        tvWorkSites.setOnItemClickListener((parent, view, position, id) -> {
            if (position >= 0) {
                STORES stores = storesArrayAdapter.getItem(position);
                if (stores != null) {
                    edDestLoc.setText(stores.STORE_CODE);
                    edDestLoc.setError(null);
                }
            }
        });


        edDestLoc = findViewById(R.id.edDestLoc);
        MaterialButton btnNext = findViewById(R.id.btnNext);
        btnNext.setOnClickListener(v -> onNextClick());

        GetStores();
        edDocNo.requestFocus();
    }

    private void GetStores() {
        List<STORES> storesList = null;
        MastersDBManager mastersDBManager = new MastersDBManager(AppVariables.getMastersDb());
        if (mastersDBManager != null) {
            mastersDBManager.open();
            storesList = mastersDBManager.getStores();
            mastersDBManager.close();
        } else {
            storesList = new ArrayList<>();
        }

        storesArrayAdapter = new ArrayAdapter<>(this, R.layout.dropdown_menu_popup_item, storesList);
        tvWorkSites.setAdapter(storesArrayAdapter);
    }

    private void onNextClick() {
        try {
            String binNo = Objects.requireNonNull(edDocNo.getText()).toString().trim().replace("\r", "").replace("\n", "");
            String destLoc = Objects.requireNonNull(edDestLoc.getText()).toString().trim().replace("\r", "").replace("\n", "");

            if (binNo.isEmpty()) {
                showToast("Invalid Bin No. cannot be empty");
                edDocNo.setError("Invalid Bin No. cannot be empty");
                edDocNo.requestFocus();
                return;
            }

            if (destLoc.isEmpty()) {
                showToast("Destination cannot be empty");
                edDestLoc.setError("Destination cannot be empty");
                edDestLoc.requestFocus();
                return;
            }

            try {
                STORES stores;
                MastersDBManager mastersDBManager = new MastersDBManager(AppVariables.getMastersDb());
                if (mastersDBManager != null) {
                    mastersDBManager.open();
                    stores = mastersDBManager.getStore(destLoc);
                    mastersDBManager.close();
                } else {
                    showToast("Cannot validate Destination");
                    return;
                }

                if (stores == null) {
                    final AlertDialog.Builder alert = new AlertDialog.Builder(this);
                    alert.setTitle("Info");
                    alert.setMessage("Store code not found in Masters. Proceed");
                    alert.setPositiveButton("OK", (dialog, whichButton) -> {
                        FinishAndMove(binNo, destLoc);
                    });
                    alert.setNegativeButton("Cancel", (dialog, whichButton) -> {
                        // nothing
                    });
                    alert.show();
                } else {
                    FinishAndMove(binNo, destLoc);
                }
            } catch (SQLException e) {
                Log.d(App.TAG, e.toString());
                showToast(e.getMessage());
            }
        } catch (Exception e) {
            Log.d(App.TAG, e.toString());
            showToast(e.getMessage());
        }
    }

    private void FinishAndMove(String binNo, String destLoc) {
        CustomResult customResult = Utils.CheckBinsForUser(binNo, binMode, destLoc, AppVariables.CurrentUser.LOGIN_NAME);
        BINS bins;
        if (customResult != null && customResult.errCode.equals("E")) {
            bins = new BINS();
            bins.BIN_MODE = binMode;
            bins.BIN_NO = binNo;
            bins.STORE_CODE = AppVariables.CurrentStore.STORE_CODE;
            bins.DEST_LOC = destLoc;
            bins.USER_NAME = AppVariables.CurrentUser.LOGIN_NAME;
            App.getDatabaseClient().getAppDatabase().genericDao().insertBin(bins);
        }

        bins = App.getDatabaseClient().getAppDatabase().genericDao().getBinByBinNoMode2(binNo, destLoc, AppVariables.CurrentUser.LOGIN_NAME);

        int bin_id = bins == null ? -1 : bins.PK_ID;
        Intent intent = this.getIntent();
        intent.putExtra(AppConstants.SELECTED_ID, bin_id);
        if (bin_id > 0) {
            setResult(RESULT_OK, intent);
        } else {
            showToast("Unable to save bin");
            setResult(RESULT_CANCELED, intent);
        }
        finishAndRemoveTask();
    }
}
