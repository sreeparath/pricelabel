package com.dcode.pricelabel.ui.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Vibrator;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.dcode.pricelabel.common.AppConstants;
import com.dcode.pricelabel.ui.dialog.CustomProgressDialog;

public abstract class BaseActivity extends AppCompatActivity {

    protected int selectedID;
    protected String selectedCode;
    protected String selectedName;
    protected AlertDialog alertDialog;
    protected CustomProgressDialog progressDialog;

    public static int convertToInt(String value, int defValue) {
        //String userdata = /*value from gui*/
        int val;
        try {
            val = Integer.parseInt(value);
        } catch (NumberFormatException nfe) {
            // bad data - set to sentinel
            val = defValue;
        }
        return val;
    }

    protected abstract int getLayoutResourceId();

    protected abstract void onViewReady(Bundle savedInstanceState);

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResourceId());
        onViewReady(savedInstanceState);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

/*    protected void setupToolBar(String screenTitle) {
        Toolbar toolbar = findViewById(R.id.toolbar);
        ((TextView) toolbar.findViewById(R.id.tv_toolbar)).setText(screenTitle);
        setSupportActionBar(toolbar);
    }*/

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        if (resultCode != Activity.RESULT_OK) return;

        selectedID = intent.getIntExtra(AppConstants.SELECTED_ID, -1);
        selectedCode = intent.getStringExtra(AppConstants.SELECTED_CODE);
        selectedName = intent.getStringExtra(AppConstants.SELECTED_NAME);
    }

    protected void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }


    protected void showAlert(String title, String message, final boolean isFinish) {
        if (alertDialog != null && alertDialog.isShowing())
            return;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        alertDialog = builder.setTitle(title).setMessage(message).setPositiveButton("OK", (dialog, which) -> {
            if (isFinish) {
                finish();
            }
        }).create();
        alertDialog.setCancelable(isFinish);
        alertDialog.show();
        shortVibration();
    }

    protected void showProgress(boolean isCancelable) {
        if (progressDialog != null && progressDialog.isShowing())
            return;

        progressDialog = new CustomProgressDialog(this, isCancelable);
        progressDialog.show();
    }

    protected void updateProgressMessage(String message) {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.updateMessage(message);
        }
    }

    protected void dismissProgress() {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

/*    protected void OnLookupClick(int master_id, int parent_id, @Nullable int[] array) {
        Intent intent = new Intent(this, SearchActivity.class);
        intent.putExtra(AppConstants.MASTER_ID, master_id);
        intent.putExtra(AppConstants.PARENT_ID, parent_id);

        if (array != null) {
            intent.putExtra(AppConstants.FILTER_KEY_INT_ARRAY, array);
        }
        startActivityForResult(intent, master_id);
    }*/

    protected void shortVibration() {
        Vibrator vibe = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        if (vibe != null && vibe.hasVibrator()) vibe.vibrate(10);
    }
}
