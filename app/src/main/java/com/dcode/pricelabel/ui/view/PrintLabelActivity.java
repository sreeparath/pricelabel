package com.dcode.pricelabel.ui.view;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageButton;

import com.dcode.pricelabel.App;
import com.dcode.pricelabel.R;
import com.dcode.pricelabel.common.AppConstants;
import com.dcode.pricelabel.common.AppVariables;
import com.dcode.pricelabel.common.CustomResult;
import com.dcode.pricelabel.common.PrintManager;
import com.dcode.pricelabel.common.Utils;
import com.dcode.pricelabel.data.MastersDBManager;
import com.dcode.pricelabel.data.model.BINS;
import com.dcode.pricelabel.data.model.ITEMS;
import com.dcode.pricelabel.data.model.Templates;
import com.dcode.pricelabel.network.model.GenericRetResponse;
import com.dcode.pricelabel.network.service.ServiceUtils;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class PrintLabelActivity extends BaseActivity {
    private TextInputEditText edDocNo;
    private TextInputEditText edDestLoc;
    private AutoCompleteTextView edTemplates;
    private ArrayAdapter<Templates> storesArrayAdapter;
    private int binMode;
    private TextInputEditText edScanCode;
    private TextInputEditText edItemDesc;
    private TextInputEditText edOldPrice;
    private TextInputEditText edNewPrice;
    private MastersDBManager itemsDB;
    private Templates template;
    private ITEMS items;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.fragment_print_label;
    }

    @Override
    protected void onViewReady(Bundle savedInstanceState) {
        binMode = 2;

        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );

        edScanCode = findViewById(R.id.edScanCode);
        edScanCode.requestFocus();
        edScanCode.setOnKeyListener((View v, int keyCode, KeyEvent event) -> {
            if(keyCode == KeyEvent.KEYCODE_ENTER){
                if(event.getAction() == KeyEvent.ACTION_DOWN){

                    return onScanCodeKeyEvent();
                }else{
                    return true;
                }
            }else{
                return false;
            }
        });

        edItemDesc = findViewById(R.id.edItemDesc);
        edOldPrice = findViewById(R.id.edOld);
        edNewPrice = findViewById(R.id.edNew);



        MaterialButton btnPrint = findViewById(R.id.btnPrint);
        btnPrint.setOnClickListener(v -> onNextClick());
        MaterialButton btnClose = findViewById(R.id.btnClose);
        btnClose.setOnClickListener(v -> finishAndRemoveTask());
        ImageButton settings = findViewById(R.id.imgSettings);
        settings.setOnClickListener(v -> onSettingsClick());

        edTemplates = findViewById(R.id.edTemplates);
        GetTemplates();

        Log.d("db##",AppVariables.getMastersDb());
        File masterDB = new File(AppVariables.getMastersDb());
        if (!masterDB.exists()) {
            showToast("Master database not found.");
            return;
        }
        itemsDB = new MastersDBManager(AppVariables.getMastersDb());

    }

    private void onSettingsClick() {
        Intent intent = new Intent(PrintLabelActivity.this, SettingsActivity.class);
        intent.putExtra("PARENT","PRINT_SCREEN");
        startActivity(intent);
        finish();
    }

    private void GetTemplates() {
        ArrayList tempList = new ArrayList<Templates>();
        Templates temp;

//        for(int i=1;i<=3;i++){
//            int id = getResources().getIdentifier("temp"+i, "string",this.getPackageName());
//            temp = new Templates();
//            temp.Code = getString(id);
//            temp.Desc ="temp"+i;
//            tempList.add(temp);
//
//        }

        String[] formatData = getResources().getStringArray(R.array.lable_formats);
        String[] formatKey =  getResources().getStringArray(R.array.lable_formats_key);
        for (int i = 0; i < formatKey.length; i++) {
            temp = new Templates();
            temp.Code = formatData[i];
            temp.Desc = formatKey[i];
            tempList.add(temp);
        }

        Log.d("array~",tempList.toString());



        storesArrayAdapter = new ArrayAdapter<>(PrintLabelActivity.this, R.layout.dropdown_menu_popup_item, tempList);
        edTemplates.setAdapter(storesArrayAdapter);
        edTemplates.setThreshold(100);
        edTemplates.setOnItemClickListener((parent, view, position, id) -> {
            if (position >= 0) {
                template = storesArrayAdapter.getItem(position);
            } else {
                template = null;
            }
        });
    }

    private boolean onScanCodeKeyEvent() {

        String ScanCode = edScanCode.getText().toString();

        if (ScanCode.isEmpty() || ScanCode.trim().length() == 0) {
            showToast("Invalid bar code");
            return false;
        }
        resetUI();
        String networkType = AppVariables.getNetwork();
        if(networkType.length()==0){
            showToast("Invalid network type!");
            return false;
        }
        if(networkType.equalsIgnoreCase(getString(R.string.offline))){
            getItemOffline(ScanCode);
        }else if(networkType.equalsIgnoreCase(getString(R.string.online))){
            getItemOnline(ScanCode);
        }
        edScanCode.requestFocus();
        edScanCode.setText("");

        return true;
    }


    private void updateUI(ITEMS items){
        edItemDesc.setText(items.LONGDESC);
        edOldPrice.setText(String.valueOf(items.OLD_PRICE));
        edNewPrice.setText(String.valueOf(items.NEW_PRICE));
    }

    private ITEMS getItemOffline(String ScanCode){
        if (itemsDB != null) {
            itemsDB.open();
        }
        ITEMS items = itemsDB.getItemsByBarcode(ScanCode);
        if(items==null){
            showToast("Invalid Item!");
            return null;
        }
        updateUI(items);
        itemsDB.close();
        return items;
    }

    private ITEMS getItemOnline(String ScanCode){

        JsonObject requestObject = ServiceUtils.ItemData.getItemData("1", ScanCode);

            showProgress(false);
            App.getNetworkClient().getAPIService().getGenericRet(requestObject, new Callback<GenericRetResponse>() {
                @Override
                public void success(GenericRetResponse genericRetResponse, Response response) {
                    dismissProgress();
                    if (genericRetResponse.getErrCode().equals("S")) {
                        String xmlDoc = genericRetResponse.getXmlDoc();
                        try {
                            ITEMS[] itemsData = new Gson().fromJson(xmlDoc, ITEMS[].class);
                            Log.d("itemsData##","itemsData##");
                            if (itemsData==null || itemsData.length<=0) {
                                showToast(" Error: No data received."+ ScanCode);
                                return;
                            }
                            updateUI(itemsData[0]);
                        } catch (Exception e) {
                            Log.d(App.TAG, e.toString());
                            e.printStackTrace();
                        }
                        dismissProgress();

                    } else {
                        String msg = genericRetResponse.getErrMessage();
                        Log.d(App.TAG, msg);
                        showToast(msg);
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    dismissProgress();
                    String msg = error.getMessage();
                    Log.d(App.TAG, msg);
                    showToast(msg);
                }
            });
return  items;
}

    private void resetUI(){
        edScanCode.requestFocus();
        edItemDesc.setText("");
        edOldPrice.setText("");
        edNewPrice.setText("");
        edScanCode.setText("");

    }

    private void printLabel() {

        if (template == null) {
            showToast("Please select label format!");
            return ;
        }

        String newPrice = edNewPrice.getText().toString();
        if(newPrice.length()==0){
            showToast("Old Price shouldn't be empty!");
            return ;
        }

        String printCode = template.Code;
        printCode = printCode.replace("{ITEM}", items.LONGDESC);
        printCode = printCode.replace("{OLD}", String.valueOf(items.OLD_PRICE));
        printCode = printCode.replace("{NEW}", String.valueOf(items.NEW_PRICE));

        String printType = AppVariables.getPrinterType();
        if(printType.equalsIgnoreCase(getString(R.string.ip))){
            ipPrint(printCode);
        }else if(printType.equalsIgnoreCase(getString(R.string.blue))){
            bluetoothPrint(printCode);
        }

    }


    private void bluetoothPrint(String printLabel){
        String mac = AppVariables.getMacAddress();
        Log.d("mac~~",mac);

        if (mac != null && mac.length() != 12 /* && !AppVariables.getMacAddress().contains(":")*/) {
            showToast("Provide a valid printer mac address!");
            return;
        }
        PrintManager.PrintMacID = mac;// getResources().getString(R.string.deviceMac);
        try {
            String result = "";
            if (printLabel.length() > 0) {
                result = PrintManager.TestPrint(printLabel);
            }else{
                result = "No Label format found!";
            }
            if (result.length() > 0)
                showToast( result);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void ipPrint(String printLabel){
        try {
            AsyncTask.execute(new Runnable() {
                @Override
                public void run() {
                    DoPrint(printLabel);
                }
            });
        }  catch (Exception e) {
            showAlert("Error",e.getMessage(),false);
        }
    }

    private void DoPrint(String printLabel) {
        try {
            String printerIP = AppVariables.getIpAddress();
            int printerPort = Integer.parseInt(AppVariables.getPort());


            String response = null;
            Socket socket = new Socket(printerIP, printerPort);
            if (socket.isConnected()) {
                PrintWriter out = new PrintWriter(new BufferedWriter(
                        new OutputStreamWriter(socket.getOutputStream())), true);
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(socket.getInputStream()));
                out.println(printLabel);
                String resposeFromServer = in.readLine();
                out.close();
                in.close();
                response = resposeFromServer;
                socket.close();
            } else {
                Log.d("Soket issue##", "Socket is not connected");
            }

        } catch (Exception e) {
            showAlert("Error",e.getMessage(),false);
        }
    }



    private void onNextClick() {
        try {
            printLabel();
        } catch (Exception e) {
            Log.d(App.TAG, e.toString());
            showToast(e.getMessage());
        }
    }

    private void FinishAndMove(String binNo, String destLoc) {
        CustomResult customResult = Utils.CheckBinsForUser(binNo, binMode, destLoc, AppVariables.CurrentUser.LOGIN_NAME);
        BINS bins;
        if (customResult != null && customResult.errCode.equals("E")) {
            bins = new BINS();
            bins.BIN_MODE = binMode;
            bins.BIN_NO = binNo;
            bins.STORE_CODE = AppVariables.CurrentStore.STORE_CODE;
            bins.DEST_LOC = destLoc;
            bins.USER_NAME = AppVariables.CurrentUser.LOGIN_NAME;
            App.getDatabaseClient().getAppDatabase().genericDao().insertBin(bins);
        }

        bins = App.getDatabaseClient().getAppDatabase().genericDao().getBinByBinNoMode2(binNo, destLoc, AppVariables.CurrentUser.LOGIN_NAME);

        int bin_id = bins == null ? -1 : bins.PK_ID;
        Intent intent = this.getIntent();
        intent.putExtra(AppConstants.SELECTED_ID, bin_id);
        if (bin_id > 0) {
            setResult(RESULT_OK, intent);
        } else {
            showToast("Unable to save bin");
            setResult(RESULT_CANCELED, intent);
        }
        finishAndRemoveTask();
    }
}
