package com.dcode.pricelabel.ui.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.navigation.Navigation;

import com.dcode.pricelabel.R;

public class HomeFragment
        extends BaseFragment {

    View root;
    ConstraintLayout cl_lable_print;
    ConstraintLayout cl_stock_take;
    ConstraintLayout cl_export;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        super.onCreateView(inflater, container, savedInstanceState);

        root = inflater.inflate(R.layout.fragment_home, container, false);

        cl_lable_print = root.findViewById(R.id.cl_price_check);
        TextView tv_bg_lable_print = root.findViewById(R.id.tv_bg_lable_print);
        tv_bg_lable_print.setOnClickListener(v -> OnModuleButtonClick(R.id.action_nav_home_to_nav_price_check));

        cl_stock_take = root.findViewById(R.id.cl_stock_take);
        TextView tv_bg_stock_take = root.findViewById(R.id.tv_bg_stock_take);
        tv_bg_stock_take.setOnClickListener(v -> OnModuleButtonClick(R.id.action_nav_home_to_nav_stock_take));

        cl_export = root.findViewById(R.id.cl_export);
        TextView tv_bg_export = root.findViewById(R.id.tv_bg_export);
        tv_bg_export.setOnClickListener(v -> OnModuleButtonClick(R.id.action_nav_home_to_nav_export));

//        TextView tv_bg_settings = root.findViewById(R.id.tv_bg_settings);
//        tv_bg_settings.setOnClickListener(v -> OnModuleButtonClick(R.id.action_nav_home_to_nav_settings));

        TextView tv_bg_logout = root.findViewById(R.id.tv_bg_logout);
        tv_bg_logout.setOnClickListener(v -> OnLogoutClick());

        /*switch (AppVariables.CurrentUser.USER_GROUP) {
            case "A":
                cl_lable_print.setVisibility(View.GONE);
                cl_stock_take.setVisibility(View.VISIBLE);
                cl_export.setVisibility(View.VISIBLE);
                break;
            case "B":
                cl_lable_print.setVisibility(View.GONE);
                cl_stock_take.setVisibility(View.GONE);
                cl_export.setVisibility(View.VISIBLE);
                break;
            case "C":
                cl_lable_print.setVisibility(View.VISIBLE);
                cl_stock_take.setVisibility(View.VISIBLE);
                cl_export.setVisibility(View.VISIBLE);
                break;
            default:
                cl_lable_print.setVisibility(View.GONE);
                cl_stock_take.setVisibility(View.GONE);
                cl_export.setVisibility(View.GONE);
        }*/

        return root;
    }

    private void OnModuleButtonClick(int action_id) {
        Navigation.findNavController(root).navigate(action_id);
    }

    private void OnLogoutClick() {
        ((MainActivity) requireActivity()).SignOut();
    }
}
