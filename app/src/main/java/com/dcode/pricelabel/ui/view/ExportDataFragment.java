package com.dcode.pricelabel.ui.view;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaScannerConnection;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SearchView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.pricelabel.App;
import com.dcode.pricelabel.R;
import com.dcode.pricelabel.adapter.PriceExportDataAdapter;
import com.dcode.pricelabel.adapter.StockExportDataAdapter;
import com.dcode.pricelabel.common.AppVariables;
import com.dcode.pricelabel.common.Utils;
import com.dcode.pricelabel.data.model.BFO_ITEM;
import com.dcode.pricelabel.data.model.BFO_PRICE_CHECK;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ExportDataFragment
        extends BaseFragment
        implements View.OnClickListener,
        SearchView.OnQueryTextListener {

    BFO_ITEM stockItem;
    BFO_PRICE_CHECK priceItem;
    List<BFO_ITEM> stockItemList;
    List<BFO_PRICE_CHECK> priceItemList;
    private TextInputEditText edRefNo;
    private TextInputEditText edRefDate;
    private TextInputEditText edVehicle;
    private TextInputEditText edDriver;
    private TextInputEditText edHelper;
    private TextInputEditText edRemarks;
    private TextInputEditText edReceiptNo;
    private TextInputEditText edSearch;
    private TextView emptyText;
    private View root;
    private StockExportDataAdapter stockExportDataAdapter;
    private PriceExportDataAdapter priceExportDataAdapter;
    private FloatingActionButton proceed;
    private TextInputEditText edPostRefNo;
    private TextInputEditText edPostRemarks;
    private MaterialButton btnExport;
    private RadioGroup radioType;
    private RadioButton radiobtnType;
    private Drawable icon;
    private final ColorDrawable background = new ColorDrawable(Color.RED);
    private RecyclerView recyclerView,recyclerViewPrice;
    private TextView edTotlQty;
    private View stockHead;
    private View priceHead;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        if (this.getArguments() != null) {
            ModuleID = this.getArguments().getInt("ModuleID", 0);

            Bundle bundle = this.getArguments();
            if (bundle != null) {
               // pl_hdr = (LS_HDR) bundle.getSerializable(AppConstants.SELECTED_OBJECT);
            }
        }

        root = inflater.inflate(R.layout.fragment_export_data, container, false);
        icon = getResources().getDrawable(R.drawable.ic_delete_black);
        setupView();
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView = root.findViewById(R.id.recyclerView);
        ((DefaultItemAnimator) Objects.requireNonNull(recyclerView.getItemAnimator())).setSupportsChangeAnimations(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        recyclerViewPrice = root.findViewById(R.id.recyclerViewPrice);
        ((DefaultItemAnimator) Objects.requireNonNull(recyclerViewPrice.getItemAnimator())).setSupportsChangeAnimations(false);
        recyclerViewPrice.setLayoutManager(new LinearLayoutManager(getContext()));

        stockExportDataAdapter = new StockExportDataAdapter(new ArrayList<>(), this);
        recyclerView.setAdapter(stockExportDataAdapter);

        recyclerViewPrice = root.findViewById(R.id.recyclerViewPrice);
        priceExportDataAdapter = new PriceExportDataAdapter(new ArrayList<>(), this);
        recyclerViewPrice.setAdapter(priceExportDataAdapter);

        edSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(stockExportDataAdapter.getItemCount()>0){
                    stockExportDataAdapter.getFilter().filter(s);
                }
                if(priceExportDataAdapter.getItemCount()>0){
                    priceExportDataAdapter.getFilter().filter(s);
                }

            }
        });

       /* ItemTouchHelper.SimpleCallback simpleCallback =
                new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

                    @Override
                    public boolean onMove(@NonNull RecyclerView recyclerView,
                                          @NonNull RecyclerView.ViewHolder viewHolder,
                                          @NonNull RecyclerView.ViewHolder viewHolder1) {
                        return false;
                    }

                    @Override
                    public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                        if (direction == ItemTouchHelper.LEFT || direction == ItemTouchHelper.RIGHT) {
                            int position = viewHolder.getAdapterPosition();
                            BFO_ITEM pl_item_rec = stockExportDataAdapter.getItemByPosition(position);
                            if (pl_item_rec != null) {
                                final AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
                                alert.setTitle("Delete this batch?");
                                alert.setPositiveButton("OK", (dialog, whichButton) -> {
                                    App.getDatabaseClient().getAppDatabase().genericDao().deleteStockItemBFo(pl_item_rec.SL_NO);
                                    recyclerView.removeViewAt(position);
                                    stockExportDataAdapter.notifyItemRemoved(position);
                                    stockExportDataAdapter.deleteBatch(position);
                                    stockExportDataAdapter.notifyItemRangeChanged(position, stockExportDataAdapter.getItemCount());
                                });
                                alert.setNegativeButton("Cancel", (dialog, whichButton) -> {
                                    //Put actions for CANCEL button here, or leave in blank
                                    stockExportDataAdapter.notifyDataSetChanged();
                                });
                                alert.show();
                            }
                            //UpdateBatchView();
                        }
                    }

                    @Override
                    public void onChildDraw(@NonNull Canvas c,
                                            @NonNull RecyclerView recyclerView,
                                            @NonNull RecyclerView.ViewHolder viewHolder,
                                            float dX, float dY, int actionState, boolean isCurrentlyActive) {
                        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
                        View itemView = viewHolder.itemView;
                        int backgroundCornerOffset = 20;

                        int iconMargin = (itemView.getHeight() - icon.getIntrinsicHeight()) / 2;
                        int iconTop = itemView.getTop() + (itemView.getHeight() - icon.getIntrinsicHeight()) / 2;
                        int iconBottom = iconTop + icon.getIntrinsicHeight();

                        if (dX > 0) {
                            int iconLeft = itemView.getLeft() + iconMargin + icon.getIntrinsicWidth();
                            int iconRight = itemView.getLeft() + iconMargin;
                            icon.setBounds(iconLeft, iconTop, iconRight, iconBottom);

                            background.setBounds(itemView.getLeft(), itemView.getTop(),
                                    itemView.getLeft() + ((int) dX) + backgroundCornerOffset, itemView.getBottom());
                        } else if (dX < 0) {
                            int iconLeft = itemView.getRight() - iconMargin - icon.getIntrinsicWidth();
                            int iconRight = itemView.getRight() - iconMargin;
                            icon.setBounds(iconLeft, iconTop, iconRight, iconBottom);

                            background.setBounds(itemView.getRight() + ((int) dX) - backgroundCornerOffset,
                                    itemView.getTop(), itemView.getRight(), itemView.getBottom());
                        } else {
                            background.setBounds(0, 0, 0, 0);
                        }
                        background.draw(c);
                        icon.draw(c);
                    }
                };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);*/

        //DownloadPL_Items();
        //loadData();
    }

    private void setupView() {
        edSearch = root.findViewById(R.id.search);
        edSearch.setVisibility(View.INVISIBLE);

        stockHead = root.findViewById(R.id.headerStock);
        priceHead = root.findViewById(R.id.headerPrice);
        stockHead.setVisibility(View.VISIBLE);
        priceHead.setVisibility(View.INVISIBLE);

        radioType = root.findViewById(R.id.radioType);
        radioType.setOnCheckedChangeListener (new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int selectedId = radioType.getCheckedRadioButtonId();
                radiobtnType = root.findViewById(selectedId);
                if (radiobtnType.getText().toString().equals("Stock Data")) {
                    //loadData("STOCK");
                    loadStockData();
                } else if (radiobtnType.getText().toString().equals("Price Data")) {
                    loadPriceData();
                }
            }
        });
        edTotlQty = root.findViewById(R.id.edTotlQty);

        btnExport = root.findViewById(R.id.btnExport);
        btnExport.setEnabled(false);
        btnExport.setOnClickListener(v -> onExportClick());

}



    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        stockExportDataAdapter.getFilter().filter(newText);
        String filterString = newText;
        return false;
    }

    @Override
    public void onClick(View view) {

    }


    private void loadStockData(){
        stockHead.setVisibility(View.VISIBLE);
        priceHead.setVisibility(View.INVISIBLE);
        recyclerViewPrice.setVisibility(View.INVISIBLE);
        showProgress(false);
        stockItemList = App.getDatabaseClient().getAppDatabase().genericDao().getStockSummary();
        stockExportDataAdapter.addItems(stockItemList);
        edTotlQty.setText("Total Scan Qty  : "+stockExportDataAdapter.getScannedCount());
        recyclerView.setVisibility(View.VISIBLE);
        btnExport.setEnabled(true);
        dismissProgress();
    }

    private void loadPriceData(){
        stockHead.setVisibility(View.INVISIBLE);
        priceHead.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.INVISIBLE);
        showProgress(false);
        priceItemList = App.getDatabaseClient().getAppDatabase().genericDao().getPriceCheckItems();
        //Log.d("stockItemList## ",String.valueOf(stockItemList.size()));
        priceExportDataAdapter.addItems(priceItemList);
        edTotlQty.setText("Total Scan Qty  : "+priceExportDataAdapter.getScannedCount());
        recyclerViewPrice.setVisibility(View.VISIBLE);
        btnExport.setEnabled(true);
        dismissProgress();
    }


    private void onExportClick(){
        if (WriteToFile()) {
            //App.getDatabaseClient().getAppDatabase().genericDao().deleteAllStockItemBFo();
            String type= radiobtnType.getText().toString().equals("Stock Data") ? "STOCK" : "PRICE";
            if(type!=null && type.equals("STOCK")){
                App.getDatabaseClient().getAppDatabase().genericDao().deleteAllStockItemBFo();
                stockExportDataAdapter = new StockExportDataAdapter(new ArrayList<>(), this);
                recyclerView.setAdapter(stockExportDataAdapter);
            }else if(type!=null && type.equals("PRICE")){
                App.getDatabaseClient().getAppDatabase().genericDao().deleteAllBFoPriceCheck();
                priceExportDataAdapter = new PriceExportDataAdapter(new ArrayList<>(), this);
                recyclerViewPrice.setAdapter(priceExportDataAdapter);
            }
        }
    }

    private boolean WriteToFile() {
        try {


            File filePath = new File(AppVariables.getOutFilesPath());
            if (!filePath.exists()) {
                boolean mkdir = filePath.mkdir();

                if (!mkdir) {
                    showToast("Failed: create output folder");
                    return false;
                }
            }

            showProgress(false);
            String fileTimeStamp = Utils.GetCurrentDateTime(Utils.FILE_NAME_FORMAT);
            String itemsFileName="";
            String headerLine = "";



            try {
                String itemLine = "";
                if (radiobtnType.getText().toString().equals("Stock Data")) {
                    String itemLineTemplate = "%s, %s, %s, %s, %s";
                    itemsFileName = String.format("%s/ST_%s_%s.csv", filePath, AppVariables.CurrentStore.STORE_CODE, fileTimeStamp);
                    headerLine = ("Barcode,    ItemDesc, Size, Price,  Total Qty\n");
                    if (stockItemList.size() > 0) {
                        Writer itemsOutput = new BufferedWriter(new FileWriter(itemsFileName, true));
                        itemsOutput.append(headerLine);
                        for (BFO_ITEM item : stockItemList) {
                            itemLine = String.format(itemLineTemplate, item.BARCODE,  item.ITEM_DESCRIPTION, item.SIZE_DESCR, item.PRICE, item.QUANTITY);
                            itemsOutput.append(itemLine).append("\r\n");
                        }
                        itemsOutput.close();
                    }
                } else if (radiobtnType.getText().toString().equals("Price Data")) {
                    String itemLineTemplate = "%s, %s, %s, %s, %s, %s, %s";
                    itemsFileName = String.format("%s/PR_%s_%s.csv", filePath, AppVariables.CurrentStore.STORE_CODE, fileTimeStamp);
                    headerLine = ("Barcode,  ItemCode,  ItemDesc,  Size,  Price,  Qty, Timestamp\n");
                    if (priceItemList.size() > 0) {
                        Writer itemsOutput = new BufferedWriter(new FileWriter(itemsFileName, true));
                        itemsOutput.append(headerLine);
                        for (BFO_PRICE_CHECK item : priceItemList) {
                            itemLine = String.format(itemLineTemplate, item.BARCODE, item.ITEM_NO, item.ITEM_DESCRIPTION, item.SIZE_DESCR, item.PRICE, item.QUANTITY, item.SCAN_TIME);
                            itemsOutput.append(itemLine).append("\r\n");
                        }
                        itemsOutput.close();
                    }
                }


            } catch (IOException e) {
                Log.d(App.TAG, e.getMessage());
                return false;
            }
            MediaScannerConnection.scanFile(requireActivity(),
                    new String[]{itemsFileName }, null, null);
            dismissProgress();
            showToast("Data Exported Successfully.");
        } catch (Exception e) {
            dismissProgress();
            Log.d(App.TAG, e.getMessage());
            return false;
        }

        return true;
    }

    private void resetUI(){
        edRefNo.setText("");
        edRefDate.setText("");
        edVehicle.setText("");
        edDriver.setText("");
        edHelper.setText("");
        edRemarks.setText("");
        edReceiptNo.setText("");
    }

}
