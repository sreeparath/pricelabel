package com.dcode.pricelabel.ui.view;

import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.dcode.pricelabel.App;
import com.dcode.pricelabel.R;
import com.dcode.pricelabel.common.AppVariables;
import com.dcode.pricelabel.common.Utils;
import com.dcode.pricelabel.data.MastersDBManager;
import com.dcode.pricelabel.data.model.ITEMS;
import com.google.android.material.textfield.TextInputEditText;

import java.io.File;
import java.util.Objects;

public class ItemsVerificationFragment extends BaseFragment {

    private View root;
    private TextView tvCurrentUser;
    private TextInputEditText edBarCode;
    private ImageView imageView;
    private TextView tvItemNo;
    private TextView tvProductCode;
    private TextView tvArticleNo;
    private TextView tvBrand;
    private TextView tvItemDesc;
    private TextView tvCustom1;
    private TextView tvCustom2;
    private TextView tvMatchMsg;
    private MastersDBManager mastersDBManager;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        if (this.getArguments() != null) {
            ModuleID = this.getArguments().getInt("ModuleID", 0);
        }

        root = inflater.inflate(R.layout.fragment_items_verification, container, false);

        tvCurrentUser = root.findViewById(R.id.tvCurrentUser);
        tvItemNo = root.findViewById(R.id.tvItemNo);
        tvProductCode = root.findViewById(R.id.tvProductCode);
        tvArticleNo = root.findViewById(R.id.tvArticleNo);
        tvBrand = root.findViewById(R.id.tvBrand);
        tvItemDesc = root.findViewById(R.id.tvItemDesc);
        tvCustom1 = root.findViewById(R.id.tvCustom1);
        tvCustom2 = root.findViewById(R.id.tvCustom2);
        tvMatchMsg = root.findViewById(R.id.tvMatchMsg);
        imageView = root.findViewById(R.id.imageView);

        edBarCode = root.findViewById(R.id.edBarCode);
        edBarCode.setOnKeyListener((v, keyCode, event) -> {
            if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    keyCode == KeyEvent.KEYCODE_ENTER) {
                onBarcodeKeyEvent();
                return true;
            }
            return false;
        });

        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        tvCurrentUser.setText(String.format("User: %s", AppVariables.CurrentUser.LOGIN_NAME));

        mastersDBManager = new MastersDBManager(AppVariables.getMastersDb());
        mastersDBManager.open();
        edBarCode.requestFocus();
    }

    private void ClearUI() {
        String str = getString(R.string.empty_string);

        edBarCode.setText(str);
        tvItemNo.setText(str);
        tvArticleNo.setText(str);
        tvProductCode.setText(str);
        tvBrand.setText(str);
        tvItemDesc.setText(str);
        tvCustom1.setText(str);
        tvCustom2.setText(str);
        tvMatchMsg.setText(str);
        imageView.setImageBitmap(null);
    }


    private void onBarcodeKeyEvent() {
        String barcode = Objects.requireNonNull(edBarCode.getText()).toString();
        ITEMS items;

        if (barcode.isEmpty() || barcode.trim().length() == 0 ) {
            return;
        }

        try {
            items = mastersDBManager.getItemsByBarcode(barcode);
            if (items != null) {
               // RefreshUI(items);
            } else {
                ClearUI();
                tvMatchMsg.setText("Item not found");
            }

        } catch (Exception ex) {
            Log.d(App.TAG, ex.toString());
            ClearUI();
            tvMatchMsg.setText("Invalid barcode");
        }

        edBarCode.requestFocus();
    }

}
