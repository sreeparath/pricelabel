package com.dcode.pricelabel.ui.view;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import com.dcode.pricelabel.App;
import com.dcode.pricelabel.R;
import com.dcode.pricelabel.common.AppVariables;
import com.dcode.pricelabel.common.PermissionsManager;
import com.dcode.pricelabel.data.MastersDBManager;
import com.dcode.pricelabel.data.model.AppSettings;
import com.dcode.pricelabel.data.model.STORES;
import com.dcode.pricelabel.data.model.USERS;
import com.google.android.material.button.MaterialButton;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class LoginActivity extends BaseActivity {
    private MaterialButton btnLogin;
    private ImageButton settings;
    private EditText edUserName;
    private EditText edPassword;
    private TextView edPdtId;
    private AutoCompleteTextView tvWorkSites;
    private MastersDBManager mastersDBManager;
    private AppSettings appSettings;

    private ArrayAdapter<STORES> storesArrayAdapter;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_login;
    }

    @Override
    protected void onViewReady(Bundle savedInstanceState) {
        edUserName = findViewById(R.id.edUserName);


        edPdtId = findViewById(R.id.edPdtIdText);
        if(AppVariables.getPdtId()!=null && AppVariables.getPdtId().length()>0){
            edPdtId.setText("PDT ID : "+ AppVariables.getPdtId());
        }

        edPassword = findViewById(R.id.edPassword);
        tvWorkSites = findViewById(R.id.acWorkSites);
        btnLogin = findViewById(R.id.btSignIn);
        settings = findViewById(R.id.imgSettings);

        btnLogin.setOnClickListener(v -> {
            try {
                OnClick_Login();
            } catch (Exception e) {
                Log.d(App.TAG, e.toString());
            }
        });
        settings.setOnClickListener(v -> onClick_Settings());
        tvWorkSites.setOnItemClickListener((parent, view, position, id) -> {
            if (position >= 0) {
                AppVariables.CurrentStore = storesArrayAdapter.getItem(position);
            } else {
                AppVariables.CurrentStore = null;
            }
        });

        PermissionsManager.verifyWriteStoragePermissions(this);
        PermissionsManager.verifyWriteStoragePermissions(this);
        mastersDBManager = new MastersDBManager(AppVariables.getMastersDb());

        GetStores();
        getPdtId();
    }

    @Override
    public void onBackPressed() {
        finishAndRemoveTask();
        super.onBackPressed();
    }

    public void onClick_Settings() {
        startActivity(new Intent(this, SettingsActivity.class));
    }

    private void OnClick_Login() {
        String uname = edUserName.getText().toString().trim();

//        if (!(App.DeviceID.equals("76b28a68-1bda-4394-b481-8cd8b6746b9e")))
//        {
//            showToast("Invalid Device " + App.DeviceID);
//            return;
//        }
        if (uname.isEmpty()) {
            edUserName.setError("User name required");
            showToast("User name required");
            return;
        }

        String uPass = edPassword.getText().toString().trim();
        if (uPass.isEmpty()) {
            edPassword.setError("Password required");
            showToast("Password required");
            return;
        }

        if (AppVariables.CurrentStore == null) {
            tvWorkSites.setError("Store required");
            showToast("Store required");
            return;
        }

        showProgress(false);
        if (ValidateUser(uname, uPass)) {
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
            finish();
        } else {
            showToast("Invalid Credentials!");
        }
    }
    private void getPdtId(){
        //File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getPath()
        //        , "app_settings");

        File file = new File(AppVariables.getAppSettingFile());

        String strRlativePath = file.getAbsolutePath();
//        boolean isfileexists = file.exists();
//        if (!isfileexists)
//        {
////            Toast.makeText(getApplicationContext(), "Configuration File " + strRlativePath + " Not Exists", Toast.LENGTH_LONG).show();
////            return;
//
//            AlertDialog.Builder builder = new AlertDialog.Builder(this);
//            builder.setPositiveButton("OK", null)
//                    .setTitle("Error")
//                    .setMessage("File " + strRlativePath + " Not Found")
//                    .setCancelable(false)
//                    .setPositiveButton("Yes",
//                            new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int id) {
//                                    finish();
//                                }
//                            })
//                    .show();
//            return;
//        }


        Gson gson = new Gson();
        try
        {
            Log.d("appset##",file.getAbsolutePath());
            //BufferedReader br = new BufferedReader(new FileReader(new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getPath(),"storage.json")));
            BufferedReader br = new BufferedReader(new FileReader(file));
            appSettings = gson.fromJson(br, AppSettings.class);
            Log.d("appSettings##",appSettings.pdtId);
            //If array use like below
            //pricechkconfig[] entries = gson.fromJson(br, pricechkconfig[].class);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setPositiveButton("OK", null)
                    .setTitle("Error")
                    .setMessage("Error While Reading Config File")
                    .setCancelable(false)
                    .setPositiveButton("Yes",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    finish();
                                }
                            })
                    .show();
            return;
        }

        //ServiceGenerator.BASE_URL = pricechkconfig.ServiceURL;
        AppVariables.setPdtId(appSettings.pdtId);
        if(AppVariables.getPdtId()!=null && AppVariables.getPdtId().length()>0){
            edPdtId.setText("PDT ID : "+ AppVariables.getPdtId());
        }
    }

    private void GetStores() {
        try {
//            showProgress(true);
            File masterDB = new File(AppVariables.getMastersDb());
            if (!masterDB.exists()) {
                showToast("Master database not found.");
                return;
            }

            List<STORES> storesList = null;
            if (mastersDBManager != null) {
                mastersDBManager.open();
                storesList = mastersDBManager.getStores();
                mastersDBManager.close();
            } else {
                storesList = new ArrayList<>();
            }

            storesArrayAdapter = new ArrayAdapter<>(LoginActivity.this,
                    R.layout.dropdown_menu_popup_item, storesList);
            tvWorkSites.setAdapter(storesArrayAdapter);
        } catch (Exception e) {
            /*catch and ignore the exception until permission are granted
             * Without permission, location will not be read
             * without location cannot login
             * */
            showToast("Error accessing Stores master");
//        } finally {
//            dismissProgress();
        }
    }

    private boolean ValidateUser(String uName, String uPass) {
        boolean status = false;
        USERS curUser = null;

        try {
            showProgress(true);
            if (mastersDBManager != null) {
                mastersDBManager.open();
                curUser = mastersDBManager.getUser(uName, uPass);
                mastersDBManager.close();
            }
            if (curUser != null && curUser.LOGIN_NAME.length() > 0) {
                AppVariables.CurrentUser = curUser;
                status = true;
            }
        } catch (Exception e) {
            Log.d(App.TAG, e.toString());
            showToast("Error accessing Users Master");
        } finally {
            dismissProgress();
        }

        return status;
    }
}
