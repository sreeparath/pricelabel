package com.dcode.pricelabel.ui.view;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;

import com.dcode.pricelabel.App;
import com.dcode.pricelabel.R;
import com.dcode.pricelabel.common.AppConstants;
import com.dcode.pricelabel.common.AppVariables;
import com.dcode.pricelabel.common.PermissionsManager;
import com.dcode.pricelabel.common.Utils;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.io.File;
import java.util.Objects;
import java.util.SplittableRandom;

public class SettingsActivity extends BaseActivity {
    Button btn_save_setting;
    private TextInputEditText edDeviceID;
    private TextInputEditText edMastersData;
    private TextInputEditText edIp;
    private TextInputEditText edPort;
    private TextInputEditText edMac;
    private TextInputEditText edUrl;
    private TextInputLayout lbUrl;
    private RadioGroup rdoGroup;
    private RadioGroup rdoGroupNet;
    private LinearLayout layoutIp;
    private LinearLayout layoutBlue;
    private ImageButton settings;


    /*private static boolean isMacValid(String mac) {
        Pattern p = Pattern.compile("^([a-fA-F0-9]{2}:?){5}[a-fA-F0-9]{2}$");
        Matcher m = p.matcher(mac);
        return m.find();
    }*/

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_settings;
    }

    @Override
    protected void onViewReady(Bundle savedInstanceState) {

        /* to init static members */
        new AppVariables(this);


        btn_save_setting = findViewById(R.id.btnNext);
        btn_save_setting.setOnClickListener(v -> onSave());

        layoutIp =  findViewById(R.id.layIp);
        layoutBlue =  findViewById(R.id.layMac);
        layoutIp.setVisibility(View.GONE);
        layoutBlue.setVisibility(View.GONE);

        edDeviceID = findViewById(R.id.edDeviceID);
        String deviceID = Utils.getValue(SettingsActivity.this, AppConstants.DEV_UNIQUE_ID, false);
        edDeviceID.setText(deviceID);

        edIp = findViewById(R.id.edIpAddress);
        String ip = AppVariables.getIpAddress();
        edIp.setText(ip);

        edPort = findViewById(R.id.edPort);
        String port = AppVariables.getPort();
        edPort.setText(port);

        edMac = findViewById(R.id.edMac);
        String mac = AppVariables.getMacAddress();
        Log.d("mac##",mac);
        edMac.setText(mac);

        rdoGroup = findViewById(R.id.radioGroup);
        rdoGroup.clearCheck();
        String printTypeInt = AppVariables.getPrinterTypeInt();
        Log.d("printTypeInt##",printTypeInt);
        if(printTypeInt!=null && printTypeInt.length()>0){
            RadioButton rdoButton = (RadioButton) rdoGroup.findViewById(Integer.valueOf(printTypeInt));
            rdoButton.setChecked(true);
        }
        String printType = AppVariables.getPrinterType();
        if(printType.equalsIgnoreCase(getString(R.string.ip))){
            layoutIp.setVisibility(View.VISIBLE);
        }else if(printType.equalsIgnoreCase(getString(R.string.blue))){
            layoutBlue.setVisibility(View.VISIBLE);
        }

        edUrl =  findViewById(R.id.edUrl);
        edUrl.setText(AppVariables.getUrl());
        edUrl.setVisibility(View.GONE);
        lbUrl =  findViewById(R.id.lbUrl);
        lbUrl.setVisibility(View.GONE);
        rdoGroupNet = findViewById(R.id.radioGroupNet);
        rdoGroupNet.clearCheck();
        String nettypeInt = AppVariables.getNetworkInt();
        if(nettypeInt!=null && nettypeInt.length()>0){
            RadioButton rdoButton = (RadioButton) rdoGroupNet.findViewById(Integer.valueOf(nettypeInt));
            rdoButton.setChecked(true);
        }

        String nettype = AppVariables.getNetwork();
        if(nettype.equalsIgnoreCase(getString(R.string.online))){
            edUrl.setVisibility(View.VISIBLE);
            lbUrl.setVisibility(View.VISIBLE);
        }else if(printType.equalsIgnoreCase(getString(R.string.offline))){
            edUrl.setVisibility(View.GONE);
            lbUrl.setVisibility(View.GONE);
        }


        edMastersData = findViewById(R.id.edMastersData);
        String sMastersData = AppVariables.getMastersPath();
        if (sMastersData.isEmpty()){
            sMastersData = getString(R.string.def_masters_path);
        }
        edMastersData.setText(sMastersData);
        edMastersData.setOnClickListener(v -> getDirectoryPath(1001));



        rdoGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int check = group.getCheckedRadioButtonId();
                RadioButton rdoButton = (RadioButton) group.findViewById(check);
                if(rdoButton.getText().toString().equalsIgnoreCase(getString(R.string.ip))){
                    layoutIp.setVisibility(View.VISIBLE);
                    layoutBlue.setVisibility(View.GONE);
                }else if(rdoButton.getText().toString().equals(getString(R.string.blue))){
                    layoutIp.setVisibility(View.GONE);
                    layoutBlue.setVisibility(View.VISIBLE);
                }
            }
        });

        rdoGroupNet.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int check = group.getCheckedRadioButtonId();
                RadioButton rdoButton = (RadioButton) group.findViewById(check);
                if(rdoButton.getText().toString().equalsIgnoreCase(getString(R.string.online))){
                    edUrl.setVisibility(View.VISIBLE);
                    lbUrl.setVisibility(View.VISIBLE);
                }else if(rdoButton.getText().toString().equals(getString(R.string.offline))){
                    edUrl.setVisibility(View.GONE);
                    lbUrl.setVisibility(View.GONE);
                }
            }
        });

        PermissionsManager.verifyWriteStoragePermissions(this);
        PermissionsManager.verifyPermissions(this);

        Intent parent = getIntent();
        String parentModule = "";
        if(parent!=null){
            parentModule = parent.getStringExtra("PARENT");
        }
        if(!(parentModule!=null && parentModule.equalsIgnoreCase("PRINT_SCREEN"))){
          //  Log.d("parent##",parentModule);
            if((printType!=null && printType.length()>0) ){
                Intent intent = new Intent(getApplicationContext(), PrintLabelActivity.class);
                startActivity(intent);
                finish();
            }
        }
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode != RESULT_OK) return;

        try {
            File file = new File(Objects.requireNonNull(Objects.requireNonNull(data).getData()).getPath());
            String[] treePath = file.getAbsolutePath().split(":");
            String externalPath = "";
            if (treePath[0].contains("primary")) {
                externalPath = "/storage/emulated/0/";
            } else {
                externalPath = Utils.getExternalSdPath();
            }

            externalPath = externalPath.endsWith("/") ? externalPath : externalPath.concat("/");

            String path = externalPath+treePath[treePath.length - 1];

            switch (requestCode) {
                case 1001:
                    edMastersData.setText(path);
                    break;
                default:
                    break;
            }
        } catch (Exception e) {
            Log.d(App.TAG, e.toString());
            showToast("Some error!");

        }
    }

    private void getDirectoryPath(int REQUEST_CODE) {
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
        startActivityForResult(intent, REQUEST_CODE);
    }

    private void onSave() {



        String sMaster = edMastersData.getText().toString().trim();
        if (sMaster.isEmpty()) {
            showToast("Master data path is mandatory!");
            return;
        }

        int check = rdoGroup.getCheckedRadioButtonId();
        if (check==-1) {
            showToast("Printer Type is Mandatory!");
            return;
        }

        String ipAddr = edIp.getText().toString().trim();
        if (ipAddr.isEmpty() &&  layoutIp.getVisibility()==View.VISIBLE) {
            showToast("Ip address is mandatory!");
            return;
        }

        String port = edPort.getText().toString().trim();
        if (port.isEmpty() && layoutIp.getVisibility()==View.VISIBLE) {
            showToast("Port is mandatory!");
            return;
        }

        String mac = edMac.getText().toString().trim();
        Log.d("mac##",mac);
        if (mac.isEmpty() &&  layoutBlue.getVisibility()==View.VISIBLE) {
            showToast("Mac Address is mandatory!");
            return;
        }

        int checkNet = rdoGroupNet.getCheckedRadioButtonId();
        if (checkNet==-1) {
            showToast("Network is Mandatory!");
            return;
        }

        String url = edUrl.getText().toString().trim();
        if (url.isEmpty() &&  edUrl.getVisibility()==View.VISIBLE) {
            showToast("URL is mandatory!");
            return;
        }

        showToast("Restart application for setting to apply");


        AppVariables.setMastersPath(sMaster);
        Log.d("sMaster##",AppVariables.getMastersPath());
        Utils.SetValue(SettingsActivity.this, AppConstants.MASTERS_PATH, AppVariables.getMastersPath());

        RadioButton rdoButton = (RadioButton) rdoGroup.findViewById(check);
        AppVariables.setPrinterType(rdoButton.getText().toString());
        AppVariables.setPrinterTypeInt(String.valueOf(check));
        AppVariables.setMacAddress(mac);
        AppVariables.setIpAddress(ipAddr);
        AppVariables.setPort(port);
        AppVariables.setUrl(url);
        RadioButton rdoButtonNet = (RadioButton) rdoGroupNet.findViewById(checkNet);
        AppVariables.setNetwork(rdoButtonNet.getText().toString());
        AppVariables.setNetworkInt(String.valueOf(checkNet));
        Intent intent = new Intent(getApplicationContext(), PrintLabelActivity.class);
        startActivity(intent);
        finish();
    }
}
