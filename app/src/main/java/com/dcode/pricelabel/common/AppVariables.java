package com.dcode.pricelabel.common;

import android.content.Context;
import android.util.Log;

import com.dcode.pricelabel.App;
import com.dcode.pricelabel.R;
import com.dcode.pricelabel.data.model.STORES;
import com.dcode.pricelabel.data.model.USERS;
import com.dcode.pricelabel.ui.view.SettingsActivity;

public class AppVariables {
    public static USERS CurrentUser = null;
    public static STORES CurrentStore = null;
    private static String MASTERS_PATH;
    private static String IMAGES_PATH;
    private static String OUT_FILES_PATH;
    public static String PdtId = "";
    public static String printerType = "";
    public static String printerTypeInt = "";
    public static String ipAddress = "";
    public static String port = "";
    public static String macAddress = "";
    public static Context context;
    public static String network = "";
    public static String networkInt = "";
    public static String url = "";

    public AppVariables(Context context) {
        try {
            this.context = context;
            Utils.GetExternalSDCardPath(context);

            String sMastersData = Utils.getValue(context, AppConstants.MASTERS_PATH, false);
            if (sMastersData.isEmpty()) {
                sMastersData = Utils.getExternalSdPath().concat(context.getString(R.string.def_masters_path));
//            } else {
//                sMastersData = Utils.getExternalSdPath().concat(sMastersData);
            }
            MASTERS_PATH = sMastersData;

            String sImagesData = Utils.getValue(context, AppConstants.IMAGES_PATH, false);
            if (sImagesData.isEmpty()) {
                sImagesData = Utils.getExternalSdPath().concat(context.getString(R.string.def_images_path));
//            } else {
//                sImagesData = Utils.getExternalSdPath().concat(sImagesData);
            }
            IMAGES_PATH = sImagesData;

            String sOutFilesPath = context.getString(R.string.def_out_path); // Utils.getValue(context, AppConstants.OUT_PATH, false);
//            if (sOutFilesPath.isEmpty()) {
//                sOutFilesPath = Utils.getExternalSdPath().concat(context.getString(R.string.def_out_path));
//            } else {
//                sOutFilesPath = Utils.getExternalSdPath().concat(sOutFilesPath);
//            }
            OUT_FILES_PATH = sOutFilesPath;

            networkInt =  Utils.getValue(context,AppConstants.NETWORK_INT,false);
            network = Utils.getValue(context,AppConstants.NETWORK,false);
            printerTypeInt  = Utils.getValue(context,AppConstants.PRINTER_TYPE_INT,false);
            printerType  = Utils.getValue(context,AppConstants.PRINTER_TYPE,false);
            ipAddress  = Utils.getValue(context,AppConstants.IP_ADDRESS,false);
            port  = Utils.getValue(context,AppConstants.PORT,false);
            url  = Utils.getValue(context,AppConstants.SERVICE_URL,false);

        } catch (Exception e) {
            Log.d(App.TAG, e.toString());
        }
    }

    public static String getUrl() {
        return url;
    }

    public static void setUrl(String url) {
        Utils.SetValue(context, AppConstants.SERVICE_URL, url);
    }

    public static String getNetworkInt() {
        return networkInt;
    }

    public static void setNetworkInt(String networkInt) {
        Utils.SetValue(context, AppConstants.NETWORK_INT, networkInt);
    }

    public static String getNetwork() {
        return network;
    }

    public static void setNetwork(String network) {
        Utils.SetValue(context, AppConstants.NETWORK, network);
    }

    public static String getPdtId() {
        return PdtId;
    }

    public static void setPdtId(String pdtId) {
        PdtId = pdtId;
    }

    public static String getMastersPath() {
        return MASTERS_PATH;
    }

    public static void setMastersPath(String value) {
        MASTERS_PATH = value.endsWith("/") ? value : value + "/";
    }

    public static String getImagesPath() {
        return IMAGES_PATH;
    }

    public static void setImagesPath(String value) {
        IMAGES_PATH = value.endsWith("/") ? value : value + "/";
    }

    public static String getOutFilesPath() {
        return OUT_FILES_PATH;
    }

    public static void setOutFilesPath(String value) {
        OUT_FILES_PATH = value.endsWith("/") ? value : value + "/";
    }

    public static String getMastersDb() {
        return getMastersPath().concat("ItemMast.db");
    }

    public static String getAppSettingFile() {
        return getMastersPath().concat("AppSettings.txt");
    }

    public static String getPrinterType() {
        return printerType;
    }

    public static void setPrinterType(String printerType) {
        Utils.SetValue(context, AppConstants.PRINTER_TYPE, printerType);
    }

    public static String getIpAddress() {
        return ipAddress;
    }

    public static void setIpAddress(String ipAddress) {
        Utils.SetValue(context, AppConstants.IP_ADDRESS, ipAddress);
    }

    public static String getPort() {
        return port;
    }

    public static void setPort(String port) {
        Utils.SetValue(context, AppConstants.PORT, port);
    }

    public static String getMacAddress() {
        return Utils.getValue(context,AppConstants.MAC,false);
    }

    public static void setMacAddress(String macAddress) {
        Utils.SetValue(context, AppConstants.MAC, macAddress);
    }

    public static String getPrinterTypeInt() {
        return printerTypeInt;
    }

    public static void setPrinterTypeInt(String printerTypeInt) {
        Utils.SetValue(context, AppConstants.PRINTER_TYPE_INT, printerTypeInt);
    }

    public static String getMastersDbNew() {
        return Utils.getValue(context,AppConstants.MASTERS_PATH,false).concat("ItemMast.db");
    }

}
