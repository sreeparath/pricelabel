package com.dcode.pricelabel.common;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.Set;
import java.util.UUID;

/*import com.honeywell.mobility.print.LinePrinter;
import com.honeywell.mobility.print.LinePrinterException;
import com.honeywell.mobility.print.PrintProgressEvent;
import com.honeywell.mobility.print.PrintProgressListener;*/

/**
 * Created by mohammed.boobacker on 10/02/2018.
 */

public class PrintManager {

    public static BluetoothSocket btsocket;
    public static OutputStream btoutputstream;
    public static BluetoothDevice mDevice = null;
    public static String PrintMacID = "";
    public static String PrinterID = "";
    public static Context m_Context;
    public static String jsonCmdAttribStr = "";
    public static int TopSpacingLines = 0;
    public static int TopToLinesSpacingLines = 1;
    public static String COL_SEP = "";
    public static char SPACE_CHAR = ' ';
    public static String base64LogoPng = null;
    public static String SignString = null;
    private static String StatusMessage = "";


    public static void connectBTDevice(BluetoothDevice device) {
        try {
            // Standard SerialPortService ID
            UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");
            if (btsocket == null) {
                btsocket = device.createRfcommSocketToServiceRecord(uuid);
            }
            btsocket.connect();
            btoutputstream = btsocket.getOutputStream();
            //mInputStream = mSocket.getInputStream();
            //  beginListenForData(); * To TEST and CHECK If this method is required or not

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String TestPrint_bck(String data) throws IOException {
        StatusMessage = "Not connected to Printer";
        if (findBT()) {
            btoutputstream.write(data.getBytes());
            btoutputstream.flush();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            btsocket.close();
            btsocket = null;
            StatusMessage = "Done";
        }
        return StatusMessage;
    }


    public static String TestPrint(String data) throws IOException {
        StatusMessage = "Not connected to Printer";
        final int chunkLength = 1020;
        if (findBT()) {

            //List<String> dataChunks = Utils.SplitAsChunks(data, chunkLength) ;

            //for (String strChunk: dataChunks) {
                btoutputstream.write(("! U1 SETLP 7 0 24\n" + data).getBytes());
                btoutputstream.flush();
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            //}
//            btoutputstream.flush();
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            btsocket.close();
            btsocket = null;
            StatusMessage = "Done";
        }
        return StatusMessage;
    }


/*    public static void TestPrint() throws IOException {
        if (findBT()) {
            btoutputstream.write(printESCP().getBytes());
            btoutputstream.flush();
            btsocket.close();
            btsocket = null;
        }
    }*/

    public static boolean findBT() {
        String msg = "";
        try {
            BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

            if (mBluetoothAdapter == null) {
                msg = "No bluetooth adapter available";
                //Toast.makeText(getApplicationContext(),msg,Toast.LENGTH_LONG).show();
                StatusMessage = msg;
            }
            if (!mBluetoothAdapter.isEnabled()) {
                Intent enableBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                msg = "Bluetooth is disabled";
                //  Toast.makeText(getApplicationContext(),msg,Toast.LENGTH_LONG).show();
                //getApplicationContext().startActivity(enableBluetooth);
                StatusMessage = msg;
            }

            Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
            if (pairedDevices.size() > 0) {
                for (BluetoothDevice device : pairedDevices) {
                    Log.d("bt##",device.getAddress().replace(":",""));
                    if (PrintMacID.equals(device.getAddress()) || PrintMacID.equalsIgnoreCase(device.getAddress().replace(":",""))) {
                        mDevice = device;
                        connectBTDevice(mDevice);
                        msg = "Connection to: " + device.getName() + " is successful";
                        //  Toast.makeText(getApplicationContext(),msg,Toast.LENGTH_LONG).show();
                        return true;
                    }
                }
                StatusMessage = "No device found. Check ID";
            } else {
                msg = "No paired device found";
                //Toast.makeText(getApplicationContext(),msg,Toast.LENGTH_LONG).show();
                StatusMessage = msg;
            }
        } catch (Exception e) {
            e.printStackTrace();
            msg = msg + "Exception text " + e.getMessage();
            //Toast.makeText(getApplicationContext(),msg,Toast.LENGTH_LONG).show();
        }
        return false;
    }

/*    public static String printESCP() {
        String message = "w(    WAVELOGIX PRINT DEMO\r\n" +
                "       DUBAI\r\n" +
                "      UNITED ARAB EMIRATES\r\n" +
                "         (256)585-6389\r\n\r\n" +
                " Merchant ID: 1312\r\n" +
                " Ref #: 0092\r\n\r\n" +
                "w)      Sale\r\n" +
                "w( XXXXXXXXXXX4003\r\n" +
                " AMEX       Entry Method: Swiped\r\n\r\n\r\n" +
                " Total:               $    53.22\r\n\r\n\r\n" +
                " 12/21/12               13:41:23\r\n" +
                " Inv #: 000092 Appr Code: 565815\r\n" +
                " Transaction ID: 001194600911275\r\n" +
                " Apprvd: Online   Batch#: 000035\r\n\r\n\r\n" +
                "          Customer Copy\r\n" +
                "           Thank You!\r\n\r\n\r\n\r\n";
        return message;
    }*/

 /*

 Below code snippet will perform printing using Intermec Printer
  */

/*    public static class PrintTask extends AsyncTask<String, Integer, String> {
        private static final String PROGRESS_CANCEL_MSG = "Printing cancelled\n";
        private static final String PROGRESS_COMPLETE_MSG = "Printing completed\n";
        private static final String PROGRESS_ENDDOC_MSG = "End of document\n";
        private static final String PROGRESS_FINISHED_MSG = "Printer connection closed\n";
        private static final String PROGRESS_NONE_MSG = "Unknown progress message\n";
        private static final String PROGRESS_STARTDOC_MSG = "Start printing document\n";

        private static final char LINE_CHAR = '-';
        private static final int TotWidth = 80;//95,80

        *//**
     * Runs on the UI thread before doInBackground(Params...).
     *//*
        @Override
        protected void onPreExecute() {
            // Clears the Progress and Status text box.
            // textMsg.setText("");

            // Disables the Print button.
            // buttonPrint.setEnabled(false);
            // Disables the Sign button.
            //   buttonSign.setEnabled(false);

            // Shows a progress icon on the title bar to indicate
            // it is working on something.
            // setProgressBarIndeterminateVisibility(true);
        }

        *//**
     * This method runs on a background thread. The specified parameters
     * are the parameters passed to the execute method by the caller of
     * this task. This method can call publishProgress to publish updates
     * on the UI thread.
     *//*
        @Override
        protected String doInBackground(String... args) {
            LinePrinter lp = null;
            String sResult = null;
            String sPrinterID = args[0];
            String sMacAddr = args[1];
            String sDocNumber = "1234567890";

            if (sMacAddr.contains(":") == false && sMacAddr.length() == 12) {
                // If the MAC address only contains hex digits without the
                // ":" delimiter, then add ":" to the MAC address string.
                char[] cAddr = new char[17];

                for (int i = 0, j = 0; i < 12; i += 2) {
                    sMacAddr.getChars(i, i + 2, cAddr, j);
                    j += 2;
                    if (j < 17) {
                        cAddr[j++] = ':';
                    }
                }

                sMacAddr = new String(cAddr);
            }

            String sPrinterURI = "bt://" + sMacAddr;
            String sUserText = "";// editUserText.getText().toString();

            LinePrinter.ExtraSettings exSettings = new LinePrinter.ExtraSettings();

            exSettings.setContext(m_Context);

            PrintProgressListener progressListener =
                    new PrintProgressListener() {
                        @Override
                        public void receivedStatus(PrintProgressEvent aEvent) {
                            // Publishes updates on the UI thread.
                            publishProgress(aEvent.getMessageType());
                        }
                    };

            try {
                lp = new LinePrinter(
                        jsonCmdAttribStr,
                        sPrinterID,
                        sPrinterURI,
                        exSettings);

                // Registers to listen for the print progress events.
                lp.addPrintProgressListener(progressListener);

                //A retry sequence in case the bluetooth socket is temporarily not ready
                int numtries = 0;
                int maxretry = 2;
                while (numtries < maxretry) {
                    try {
                        lp.connect();  // Connects to the printer
                        break;
                    } catch (LinePrinterException ex) {
                        numtries++;
                        Thread.sleep(1000);
                    }
                }
                if (numtries == maxretry) lp.connect();//Final retry

                // Check the state of the printer and abort printing if there are
                // any critical errors detected.
                int[] results = lp.getStatus();
                if (results != null) {
                    for (int err = 0; err < results.length; err++) {
                        if (results[err] == 223) {
                            // Paper out.
                            throw new Exception("PaperOut");// PrintActivity.BadPrinterStateException("Paper out");
                        } else if (results[err] == 227) {
                            // Lid open.
                            throw new Exception("Printer lid open");// new PrintActivity.BadPrinterStateException("Printer lid open");
                        }
                    }
                }


//                String x = PrintDoc(lp);
                //sResult = "Number of bytes sent to printer: " + lp.getBytesWritten();
            }
//            catch (Exception ex)
//            {
//                // Stop listening for printer events.
//                lp.removePrintProgressListener(progressListener);
//                sResult = "Printer error detected: " + ex.getMessage() + ". Please correct the error and try again.";
//            }
            catch (LinePrinterException ex) {
                sResult = "LinePrinterException: " + ex.getMessage();
            } catch (Exception ex) {
                if (ex.getMessage() != null)
                    sResult = "Unexpected exception: " + ex.getMessage();
                else
                    sResult = "Unexpected exception.";
            } finally {
                if (lp != null) {
                    try {
                        lp.disconnect();  // Disconnects from the printer
                        lp.close();  // Releases resources
                    } catch (Exception ex) {
                    }
                }
            }

            // The result string will be passed to the onPostExecute method
            // for display in the the Progress and Status text box.
            return sResult;
        }

        *//**
     * Runs on the UI thread after publishProgress is invoked. The
     * specified values are the values passed to publishProgress.
     *//*
        @Override
        protected void onProgressUpdate(Integer... values) {
            // Access the values array.
            int progress = values[0];

            switch (progress) {
                case PrintProgressEvent.MessageTypes.CANCEL:
                    // textMsg.append(PROGRESS_CANCEL_MSG);
                    break;
                case PrintProgressEvent.MessageTypes.COMPLETE:
                    //  textMsg.append(PROGRESS_COMPLETE_MSG);
                    break;
                case PrintProgressEvent.MessageTypes.ENDDOC:
                    //  textMsg.append(PROGRESS_ENDDOC_MSG);
                    break;
                case PrintProgressEvent.MessageTypes.FINISHED:
                    //   textMsg.append(PROGRESS_FINISHED_MSG);
                    break;
                case PrintProgressEvent.MessageTypes.STARTDOC:
                    //  textMsg.append(PROGRESS_STARTDOC_MSG);
                    break;
                default:
                    //   textMsg.append(PROGRESS_NONE_MSG);
                    break;
            }
        }

        *//**
     * Runs on the UI thread after doInBackground method. The specified
     * result parameter is the value returned by doInBackground.
     *//*
        @Override
        protected void onPostExecute(String result) {
            // Displays the result (number of bytes sent to the printer or
            // exception message) in the Progress and Status text box.
//            if (result != null)
//            {
//                textMsg.append(result);
//            }
//
//            // Dismisses the progress icon on the title bar.
//            setProgressBarIndeterminateVisibility(false);
//
//            // Enables the Print button.
//            buttonPrint.setEnabled(true);
//            // Enables the Sign button.
//            buttonSign.setEnabled(true);
        }


    } //endofclass PrintTask*/

}
