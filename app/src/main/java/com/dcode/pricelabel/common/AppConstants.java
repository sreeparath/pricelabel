package com.dcode.pricelabel.common;

public class AppConstants {
    public static final String DEV_UNIQUE_ID = "DEV_UNIQUE_ID";
    public static final String PRINTER_MAC_ID = "PRINTER_MAC_ID";
    public static final String SERVICE_URL = "SERVICE_URL";
    public static final String PRINTER_MODEL = "PRINTER_MODEL";
    public static final String SELECTED_ID = "SELECTED_ID";
    public static final String SELECTED_CODE = "SELECTED_CODE";
    public static final String SELECTED_NAME = "SELECTED_NAME";
    public static final String PARENT_ID = "PARENT_ID";
    public static final String FILTER_KEY_INT_ARRAY = "FILTER_KEY_INT_ARRAY";
    public static final String SELECTED_OBJECT = "SELECTED_OBJECT";

    public static final String IMAGES_PATH = "IMAGES_PATH";
    public static final String MASTERS_PATH = "MASTERS_PATH";
    public static final String OUT_PATH = "OUT_PATH";
    public static final String NO_EXT_PATH = "/storage/emulated/0/Download/";
    public static final String PDT_ID = "PDT_ID";
    public static final String PRINTER_TYPE = "PRINTER_TYPE";
    public static final String PRINTER_TYPE_INT = "PRINTER_TYPE_INT";
    public static final String IP_ADDRESS = "IP_ADDRESS";
    public static final String PORT = "PORT";
    public static final String MAC = "MAC";
    public static final String NETWORK = "NETWORK";
    public static final String NETWORK_INT = "NETWORK_INT";
}
