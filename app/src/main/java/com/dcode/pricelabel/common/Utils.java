package com.dcode.pricelabel.common;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Log;

import androidx.sqlite.db.SimpleSQLiteQuery;

import com.dcode.pricelabel.App;
import com.dcode.pricelabel.data.model.GENERIC_COUNT;
import com.dcode.pricelabel.data.model.MAX_VALUE;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;

public class Utils {

    public static String DB_DATE_FORMAT = "yyyy-MMM-dd HH:mm:ss a";
    public static String FILE_NAME_FORMAT = "yyyyMMddHHmm";
    private static String EXTERNAL_SD_PATH = "/storage/emulated/0/";

    public static String getExternalSdPath() {
        return EXTERNAL_SD_PATH;
    }

    public synchronized static String getValue(Context context, String PREF_KEY, Boolean createRandom) {
        SharedPreferences sharedPrefs = context.getSharedPreferences(
                PREF_KEY, Context.MODE_PRIVATE);
        String uniqueID = sharedPrefs.getString(PREF_KEY, null);
        if ((uniqueID == null || uniqueID.length() == 0) && createRandom) {
            uniqueID = UUID.randomUUID().toString();
            SharedPreferences.Editor editor = sharedPrefs.edit();
            editor.putString(PREF_KEY, uniqueID);
            editor.apply();
        }
        if (uniqueID == null) {
            uniqueID = "";
        }
        return uniqueID;
    }

    public synchronized static boolean SetValue(Context context, String PREF_KEY, String value) {
        SharedPreferences sharedPrefs = context.getSharedPreferences(
                PREF_KEY, Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPrefs.edit();
        editor.putString(PREF_KEY, value);
        editor.apply();
        return true;
    }

    @SuppressLint("DefaultLocale")
    public static CustomResult CheckBinsForUser(String BinNo, int binMode, String destLoc, String UserName) {

        CustomResult objResult = new CustomResult();
        objResult.errCode = "S";
        objResult.errMessage = "";
        objResult.isSuccess = true;
        String formatted;

        String strTemplateMode1 = "SELECT COUNT(1) AS TOTAL_COUNT FROM BINS WHERE BIN_NO = '%s' AND BIN_MODE = %d AND USER_NAME = '%s'";
        String strTemplateMode2 = "SELECT COUNT(1) AS TOTAL_COUNT FROM BINS WHERE BIN_NO = '%s' AND BIN_MODE = %d AND DEST_LOC = '%s' AND USER_NAME = '%s'";
        if (binMode == 1) {
            formatted = String.format(strTemplateMode1, BinNo, binMode, UserName);
        } else if (binMode == 2) {
            formatted = String.format(strTemplateMode2, BinNo, binMode, destLoc, UserName);
        } else {
            return null;
        }

        SimpleSQLiteQuery squery = new SimpleSQLiteQuery(formatted);
        GENERIC_COUNT objCount = App.getDatabaseClient().getAppDatabase().genericDao().getCount(squery);
        int cnt = objCount.TOTAL_COUNT;
        if (cnt > 0) {
            objResult.errCode = "S";
            objResult.errMessage = "Some Transaction Present in " + BinNo;
            objResult.isSuccess = true;
        } else {
            objResult.errCode = "E";
            objResult.errMessage = "No Transaction Present in " + BinNo;
            objResult.isSuccess = false;
        }
        return objResult;
    }

    public static int GetItemsCount(int bin_id) {
        String sqlTemplate = "SELECT SUM(QTY) TOTAL_COUNT FROM BIN_ITEMS WHERE BIN_ID = %d";
        String formatted = String.format(Locale.getDefault(), sqlTemplate, bin_id);
        SimpleSQLiteQuery squery = new SimpleSQLiteQuery(formatted);
        GENERIC_COUNT objCount = App.getDatabaseClient().getAppDatabase().genericDao().getCount(squery);
        return objCount.TOTAL_COUNT;
    }

    public static int GetItemsCountByID(int bin_id) {
        String sqlTemplate = "SELECT COUNT(PK_ID) TOTAL_COUNT FROM BIN_ITEMS WHERE BIN_ID = %d";
        String formatted = String.format(Locale.getDefault(), sqlTemplate, bin_id);
        SimpleSQLiteQuery squery = new SimpleSQLiteQuery(formatted);
        GENERIC_COUNT objCount = App.getDatabaseClient().getAppDatabase().genericDao().getCount(squery);
        return objCount.TOTAL_COUNT;
    }

    public static int GetBinsCount() {
        String formatted = "SELECT COUNT(PK_ID) TOTAL_COUNT FROM BINS";
        SimpleSQLiteQuery squery = new SimpleSQLiteQuery(formatted);
        GENERIC_COUNT objCount = App.getDatabaseClient().getAppDatabase().genericDao().getCount(squery);
        return objCount.TOTAL_COUNT;
    }

    public static int GetTotalBinItemsCount() {
        String formatted = "SELECT SUM(QTY) TOTAL_COUNT FROM BIN_ITEMS";
        SimpleSQLiteQuery squery = new SimpleSQLiteQuery(formatted);
        GENERIC_COUNT objCount = App.getDatabaseClient().getAppDatabase().genericDao().getCount(squery);
        return objCount.TOTAL_COUNT;
    }

    public static String GetCurrentDateTime(final String outputFormat) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(outputFormat, Locale.getDefault());
        Date dateObj = new Date();
        return simpleDateFormat.format(dateObj);
    }

    private static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static Bitmap decodeSampledBitmapFromDisk(final String filePath,
                                                     int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, options);

        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(filePath, options);
    }

    private static boolean hasExternalSDCard() {
        try {
            String state = Environment.getExternalStorageState();
            if (Environment.MEDIA_MOUNTED.equals(state) || Environment.MEDIA_MOUNTED_READ_ONLY.equals(state))
                return true;
        } catch (Throwable ignored) {
        }
        return false;
    }

    public static void GetExternalSDCardPath(Context context) {
        if (hasExternalSDCard()) {
            try {
                File[] files = context.getExternalFilesDirs(null);
                if (files == null) return;

                String sdPath = "";

                // Honeywell has IPSM Card
                for (File file : files) {
                    String currPath = file.getAbsolutePath();
                    if (!currPath.contains("emulated") && !currPath.contains("IPSM")) {
                        sdPath = currPath.substring(0, currPath.indexOf("Android/"));
                        break;
                    }
                }

                if (sdPath.isEmpty()) {
                    //no external sd card, so set to Downloads folder
                    EXTERNAL_SD_PATH = AppConstants.NO_EXT_PATH;
                    return;
                }
                ;
                File sdcard = new File(sdPath);
                if (sdcard.exists()) {
                    String value = sdcard.getAbsolutePath();
                    EXTERNAL_SD_PATH = value.endsWith("/") ? value : value.concat("/");
                }
            } catch (Exception e) {
                Log.d("SDPath", e.toString());
            }
        }
    }


    public static long GetMaxValue(String TableName, String FieldName, boolean useFilter, String FilterFieldName, String FilterFieldValue) {
        String templateHasFilter = "SELECT IFNULL(MAX(%s),0) + 1 as MAX_VAL FROM %s WHERE %s = %s";
        String templateNoFilter = "SELECT IFNULL(MAX(%s),0) + 1 as MAX_VAL FROM %s ";
        String formatted = "";

        if (useFilter) {
            formatted = String.format(templateHasFilter, FieldName, TableName, FilterFieldName, FilterFieldValue);
        } else {
            formatted = String.format(templateNoFilter, FieldName, TableName);
        }

        SimpleSQLiteQuery query = new SimpleSQLiteQuery(formatted);
        MAX_VALUE objMaxVal = App.getDatabaseClient().getAppDatabase().genericDao().getMaxValue(query);
        return objMaxVal.MAX_VAL;
    }

    public static long GetMaxValue(String TableName, String FieldName, boolean useFilter, String FilterFieldName, long FilterFieldValue) {
        String templateHasFilter = "SELECT IFNULL(MAX(%s),0) as MAX_VAL FROM %s WHERE %s = %d";
        String templateNoFilter = "SELECT IFNULL(MAX(%s),0) as MAX_VAL FROM %s ";
        String formatted = "";

        if (useFilter) {
            formatted = String.format(templateHasFilter, FieldName, TableName, FilterFieldName, FilterFieldValue);
        } else {
            formatted = String.format(templateNoFilter, FieldName, TableName);
        }

        SimpleSQLiteQuery query = new SimpleSQLiteQuery(formatted);
        MAX_VALUE objMaxVal = App.getDatabaseClient().getAppDatabase().genericDao().getMaxValue(query);
        return objMaxVal.MAX_VAL;
    }

}
