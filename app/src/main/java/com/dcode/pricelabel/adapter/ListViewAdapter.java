package com.dcode.pricelabel.adapter;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.dcode.pricelabel.App;
import com.dcode.pricelabel.R;
import com.dcode.pricelabel.data.model.BIN_ITEMS;

import java.util.ArrayList;
import java.util.List;

public class ListViewAdapter
        extends BaseAdapter {
    Drawable iconDelete;
    private List<BIN_ITEMS> listItems;
    private Activity activity;

    public ListViewAdapter(Activity activity, ArrayList<BIN_ITEMS> list) {
        super();
        this.activity = activity;
        listItems = list;
    }

    @Override
    public int getCount() {
        return listItems.size();
    }

    @Override
    public Object getItem(int position) {
        return listItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;

        LayoutInflater inflater = activity.getLayoutInflater();

        if (convertView == null) {

            convertView = inflater.inflate(R.layout.column_row, null);
            holder = new ViewHolder();

            holder.txtFirst = convertView.findViewById(R.id.TextFirst);
            holder.txtSecond = convertView.findViewById(R.id.TextSecond);
            holder.txtThird = convertView.findViewById(R.id.TextThird);
            holder.txtFourth = convertView.findViewById(R.id.TextFourth);
            holder.icDelete = convertView.findViewById(R.id.imDelete);

            holder.icDelete.setOnClickListener(v -> removeItem(position));

            convertView.setTag(holder);
        } else {

            holder = (ViewHolder) convertView.getTag();
        }

        BIN_ITEMS items = listItems.get(position);
        holder.txtFirst.setText(items.ITEM_NO);
        holder.txtSecond.setText(String.format("%s-%s", items.PRODUCT_CODE, items.ITEM_NO));
        holder.txtThird.setText(items.BRAND);
        holder.txtFourth.setText(items.ITEM_DESCRIPTION);

        return convertView;
    }

    public void refreshData(int bin_id) {
        List<BIN_ITEMS> itemsList = App.getDatabaseClient().getAppDatabase().genericDao().getItemsByBin(bin_id);
        this.listItems = itemsList;
        notifyDataSetChanged();
    }

    private void removeItem(int position) {
        BIN_ITEMS item = listItems.get(position);
        App.getDatabaseClient().getAppDatabase().genericDao().deleteBinItemByID(item.PK_ID);
        listItems.remove(item);
        notifyDataSetChanged();
    }

    private class ViewHolder {
        ImageView icDelete;
        TextView txtFirst;
        TextView txtSecond;
        TextView txtThird;
        TextView txtFourth;
    }
}
