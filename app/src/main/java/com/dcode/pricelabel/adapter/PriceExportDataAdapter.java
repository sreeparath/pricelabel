package com.dcode.pricelabel.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.pricelabel.App;
import com.dcode.pricelabel.R;
import com.dcode.pricelabel.data.model.BFO_PRICE_CHECK;

import java.util.ArrayList;
import java.util.List;

public class PriceExportDataAdapter
        extends RecyclerView.Adapter<
        PriceExportDataAdapter.RecyclerViewHolder>
        implements Filterable {

    private List<BFO_PRICE_CHECK> receiptsList;
    private List<BFO_PRICE_CHECK> receiptsListFull;
    private View.OnClickListener shortClickListener;
    private Filter modelFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<BFO_PRICE_CHECK> filteredList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(receiptsListFull);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (BFO_PRICE_CHECK item : receiptsListFull) {
                    if ((item.ITEM_DESCRIPTION != null && item.ITEM_DESCRIPTION.toLowerCase().contains(filterPattern)) ||
                            (item.ITEM_NO != null && item.ITEM_NO.toLowerCase().contains(filterPattern))) {
                        filteredList.add(item);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            receiptsList.clear();
            receiptsList.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };

    public PriceExportDataAdapter(List<BFO_PRICE_CHECK> dataList, View.OnClickListener shortClickListener) {
        this.receiptsList = dataList;
        this.receiptsListFull = new ArrayList<>(dataList);
        this.shortClickListener = shortClickListener;
    }

    @Override
    public Filter getFilter() {
        return modelFilter;
    }

    @NonNull
    @Override
    public PriceExportDataAdapter.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RecyclerViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_price_check, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewHolder holder, int position) {
        final BFO_PRICE_CHECK po_det = receiptsList.get(position);
        Log.d("po_det#",po_det.toString());
        holder.tvItemDesc.setText(po_det.ITEM_DESCRIPTION);
        holder.tvItemCode.setText(po_det.ITEM_NO);
        holder.tvSize.setText(String.valueOf(po_det.SIZE_DESCR));
        holder.tvBrand.setText(String.valueOf(po_det.BRAND));
        holder.tvCat.setText(String.valueOf(po_det.CATEGOTY));
        holder.tvScale.setText(String.valueOf(po_det.SCALE));
        holder.tvUOM.setText(String.valueOf(po_det.UOM));
        holder.tvColor.setText(String.valueOf(po_det.COLOUR));

        holder.itemView.setTag(po_det);
        holder.itemView.setOnClickListener(shortClickListener);
    }

    @Override
    public int getItemCount() {
        return receiptsList.size();
    }

    public int getScannedCount() {
        int totQty = 0;
        for(BFO_PRICE_CHECK item : receiptsList){
            totQty = totQty +item.QUANTITY;
        }
        return totQty;
    }

    public BFO_PRICE_CHECK getItemByPosition(int position) {
        return this.receiptsList.get(position);
    }

    public void deleteBatch(int position) {
        try {
//            grDetListFull.remove(position);
            receiptsList.remove(position);
            notifyItemRemoved(position);
            notifyItemRangeChanged(position, receiptsList.size());
            notifyDataSetChanged();
        } catch (Exception e) {
            Log.d(App.TAG, e.toString());
        }
    }

    public void addItems(List<BFO_PRICE_CHECK> dataList) {
        this.receiptsList = dataList;
        this.receiptsListFull = new ArrayList<>(dataList);
        notifyDataSetChanged();
    }

    static class RecyclerViewHolder extends RecyclerView.ViewHolder {
        private TextView tvItemDesc;
        private TextView tvItemCode;
        private TextView tvSize;
        private TextView tvScale;
        private TextView tvUOM;
        private TextView tvColor;
        private TextView tvCat;
        private TextView tvBrand;

        RecyclerViewHolder(View view) {
            super(view);
            tvItemDesc = view.findViewById(R.id.tvItemDesc);
            tvItemCode = view.findViewById(R.id.tvItemCode);
            tvSize = view.findViewById(R.id.tvSize);
            tvScale = view.findViewById(R.id.tvScale);
            tvUOM = view.findViewById(R.id.tvUOM);
            tvColor = view.findViewById(R.id.tvColor);
            tvCat = view.findViewById(R.id.tvCat);
            tvBrand = view.findViewById(R.id.tvBrand);
        }
    }
}
