package com.dcode.pricelabel.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.dcode.pricelabel.App;
import com.dcode.pricelabel.R;
import com.dcode.pricelabel.data.model.BFO_ITEM;

import java.util.ArrayList;
import java.util.List;

public class StockTakeItemAdapter
        extends RecyclerView.Adapter<
        StockTakeItemAdapter.RecyclerViewHolder>
        implements Filterable {

    private List<BFO_ITEM> receiptsList;
    private List<BFO_ITEM> receiptsListFull;
    private View.OnClickListener shortClickListener;
    private Filter modelFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<BFO_ITEM> filteredList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(receiptsListFull);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (BFO_ITEM item : receiptsListFull) {
                    if ((item.ITEM_DESCRIPTION != null && item.ITEM_DESCRIPTION.toLowerCase().contains(filterPattern)) ||
                            (item.ITEM_NO != null && item.ITEM_NO.toLowerCase().contains(filterPattern))) {
                        filteredList.add(item);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            receiptsList.clear();
            receiptsList.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };

    public StockTakeItemAdapter(List<BFO_ITEM> dataList, View.OnClickListener shortClickListener) {
        this.receiptsList = dataList;
        this.receiptsListFull = new ArrayList<>(dataList);
        this.shortClickListener = shortClickListener;
    }

    @Override
    public Filter getFilter() {
        return modelFilter;
    }

    @NonNull
    @Override
    public StockTakeItemAdapter.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RecyclerViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_stock_take_items, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewHolder holder, int position) {
        final BFO_ITEM po_det = receiptsList.get(position);
        Log.d("po_det#",po_det.toString());
        holder.tvItemDesc.setText(po_det.ITEM_DESCRIPTION);
        holder.tvItemCode.setText(po_det.ITEM_NO);
        holder.tvSize.setText(String.valueOf(po_det.SIZE_DESCR));
        holder.tvScancode.setText(String.valueOf(po_det.BARCODE));
        holder.tvPrice.setText(String.valueOf(po_det.PRICE));

        holder.itemView.setTag(po_det);
        holder.itemView.setOnClickListener(shortClickListener);
    }

    @Override
    public int getItemCount() {
        return receiptsList.size();
    }

    public BFO_ITEM getItemByPosition(int position) {
        return this.receiptsList.get(position);
    }

    public void deleteBatch(int position) {
        try {
//            grDetListFull.remove(position);
            receiptsList.remove(position);
            notifyItemRemoved(position);
            notifyItemRangeChanged(position, receiptsList.size());
            notifyDataSetChanged();
        } catch (Exception e) {
            Log.d(App.TAG, e.toString());
        }
    }

    public void addItems(List<BFO_ITEM> dataList) {
        this.receiptsList = dataList;
        this.receiptsListFull = new ArrayList<>(dataList);
        notifyDataSetChanged();
    }

    static class RecyclerViewHolder extends RecyclerView.ViewHolder {
        private TextView tvItemDesc;
        private TextView tvItemCode;
        private TextView tvSize;
        private TextView tvScancode;
        private TextView tvPrice;

        RecyclerViewHolder(View view) {
            super(view);
            tvItemDesc = view.findViewById(R.id.tvItemDesc);
            tvItemCode = view.findViewById(R.id.itemcode);
            tvSize = view.findViewById(R.id.size);
            tvScancode = view.findViewById(R.id.scancode);
            tvPrice = view.findViewById(R.id.price);
        }
    }
}
