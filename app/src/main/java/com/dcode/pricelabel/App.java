package com.dcode.pricelabel;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import com.dcode.pricelabel.common.AppPreferences;
import com.dcode.pricelabel.common.AppVariables;
import com.dcode.pricelabel.data.DatabaseClient;
import com.dcode.pricelabel.network.NetworkClient;
import com.facebook.stetho.Stetho;

import io.github.inflationx.calligraphy3.CalligraphyConfig;
import io.github.inflationx.calligraphy3.CalligraphyInterceptor;
import io.github.inflationx.viewpump.ViewPump;

public class App extends Application {
    public static final String TAG = "ERR_TAG";
    public static String DeviceID = "12345";
    private static Context context;

    public static DatabaseClient getDatabaseClient() {
        return DatabaseClient.getInstance(context);
    }

    public static NetworkClient getNetworkClient() {
        return NetworkClient.getInstance();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;

        ViewPump.init(ViewPump.builder()
                .addInterceptor(new CalligraphyInterceptor(
                        new CalligraphyConfig.Builder()
                                .setDefaultFontPath("font/dubai_regular.ttf")
                                .setFontAttrId(R.attr.fontPath)
                                .build()))
                .build());

        DeviceID = AppPreferences.getValue(context, AppPreferences.DEV_UNIQUE_ID, true);
        Stetho.initializeWithDefaults(context);

    }

}
