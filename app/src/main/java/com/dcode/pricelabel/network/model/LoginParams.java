package com.dcode.pricelabel.network.model;

public class LoginParams {

    public String LoginName;

    public String LoginPassword;

    public int PasswordLength;

    public String SPName;

    public int PasswordEncrypted;

    public int EncryptToDB;

    public String DBName;

    public String DeviceIP;

    public String UserName;
    public String UserPassword;
}
