package com.dcode.pricelabel.network.model;

import com.google.gson.annotations.SerializedName;

public class ValidUserResponse {

    @SerializedName("FullName")
    String FullName;

    @SerializedName("LoginName")
    String LoginName;

    @SerializedName("LoginPassword")
    private String LoginPassword;

    @SerializedName("SecurityToken")
    private String SecurityToken;

    @SerializedName("TokenExpiry")
    private String TokenExpiry;

    @SerializedName("UserID")
    private String UserID;

    @SerializedName("TeamID")
    private String TeamID;

    @SerializedName("LocCode")
    private String LocCode;

    @SerializedName("LocName")
    private String LocName;

//    @SerializedName("ValidFrom")
//    private String ValidFrom;
//
//    @SerializedName("ValidTo")
//    private String ValidTo;

    public String getUserID() {
        return UserID;
    }

    public String getLoginName() {
        return LoginName;
    }

    public String getLoginPassword() {
        return LoginPassword;
    }

    public String getFullName() {
        return FullName;
    }

    public String getTeamID() {
        return TeamID;
    }

    public String getLoCode() {
        return LocCode;
    }

    public String getLoName() {
        return LocName;
    }
}
