package com.dcode.pricelabel.network.model;

import com.google.gson.annotations.SerializedName;

public class BarcodePrintersResponse {
    @SerializedName("PrinterID")
    public long PrinterID;

    @SerializedName("PrinterType")
    public String PrinterType;

    @SerializedName("FriendlyName")
    public String FriendlyName;

    @SerializedName("PrinterNameOrIP")
    public String PrinterNameOrIP;

    @SerializedName("PrinterPort")
    public int PrinterPort;

    @SerializedName("IsDefault")
    public int IsDefault;
}
