package com.dcode.pricelabel.network.service;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;


import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ServiceUtils {


    public static JsonObject createJsonObject(String paramName, String ParamType, String Value) {
        JsonObject jsonObject;
        jsonObject = new JsonObject();
        jsonObject.addProperty("Name", paramName);
        jsonObject.addProperty("ParamType", ParamType);
        jsonObject.addProperty("TypName", "");
        jsonObject.addProperty("Value", Value);
        return jsonObject;
    }

    public static JsonObject createJsonObject(String paramName, String ParamType, String ParamTypName, String Value) {
        JsonObject jsonObject;
        jsonObject = new JsonObject();
        jsonObject.addProperty("Name", paramName);
        jsonObject.addProperty("ParamType", ParamType);
        jsonObject.addProperty("TypName", ParamTypName);
        jsonObject.addProperty("Value", Value);
        return jsonObject;
    }

    public static JsonObject createJsonObject(String paramName, String ParamType, long Value) {
        JsonObject jsonObject;
        jsonObject = new JsonObject();
        jsonObject.addProperty("Name", paramName);
        jsonObject.addProperty("ParamType", ParamType);
        jsonObject.addProperty("TypName", "");
        jsonObject.addProperty("Value", Value);
        return jsonObject;
    }

    public static JsonElement GenerateCommonReadParams(long PK_ID) {
        JsonArray array = new JsonArray();
        JsonObject jsonObject;

        jsonObject = new JsonObject();
        jsonObject.addProperty("Name", "USER_ID");
        jsonObject.addProperty("ParamType", "22");
        jsonObject.addProperty("TypName", "");
        //jsonObject.addProperty("Value", App.currentUser.USER_ID);
        array.add(jsonObject);

        jsonObject = new JsonObject();
        jsonObject.addProperty("Name", "ID");
        jsonObject.addProperty("ParamType", "8");
        jsonObject.addProperty("TypName", "");
        jsonObject.addProperty("Value", PK_ID);
        array.add(jsonObject);
        return array;
    }



    public static JsonObject ValidateUserLogin(String userName, String password) {
        JsonObject jsonObject;

        JsonArray array = new JsonArray();
        jsonObject = createJsonObject("USER_NAME", "22",userName);
        array.add(jsonObject);
        jsonObject = createJsonObject("USER_PASSWORD", "22", password);
        array.add(jsonObject);

        jsonObject = new JsonObject();
        jsonObject.addProperty("SPName", "SAP.VALIDATEUSER");
        jsonObject.addProperty("DBName", "");
        jsonObject.addProperty("USER_PASSWORD", userName);
        jsonObject.addProperty("USER_NAME", password);
        //jsonObject.add("dbparams", array);
        return jsonObject;
    }

    public static class ItemData {

        public static JsonObject getItemData(final String store, final String barcode) {
            JsonObject jsonObject;

            JsonArray array = new JsonArray();
            jsonObject = createJsonObject("STORE_CODE", "22", store);
            array.add(jsonObject);
            jsonObject = createJsonObject("BARCODE", "22", barcode);
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject.addProperty("ProcName", "PDT.GET_ITEM_SPR");
            jsonObject.addProperty("DBName", "MIMS_1ERP_20201008");
            jsonObject.add("dbparams", array);
            return jsonObject;
        }




    }


}
