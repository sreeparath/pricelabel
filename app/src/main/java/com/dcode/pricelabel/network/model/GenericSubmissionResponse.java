package com.dcode.pricelabel.network.model;

import com.google.gson.annotations.SerializedName;

public class GenericSubmissionResponse {
    @SerializedName("ErrCode")
    private String errCode;
    @SerializedName("ErrMessage")
    private String errMessage;
    @SerializedName("RetID")
    private long RetID;

    public String getErrCode() {
        return errCode;
    }

    public String getErrMessage() {
        return errMessage;
    }

    public long getRetId() {
        return RetID;
    }
}
