package com.dcode.pricelabel.network.model;


import com.google.gson.annotations.SerializedName;

public class GenericRetResponse {
    @SerializedName("ErrCode")
    private String errCode;
    @SerializedName("ErrMessage")
    private String errMessage;
    @SerializedName("xmlDoc")
    private String xmlDoc;

    public String getErrCode() {
        return errCode;
    }

    public String getErrMessage() {
        return errMessage;
    }

    public String getXmlDoc() {
        return xmlDoc;
    }
}
