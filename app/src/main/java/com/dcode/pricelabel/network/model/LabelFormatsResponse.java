package com.dcode.pricelabel.network.model;

import com.google.gson.annotations.SerializedName;

public class LabelFormatsResponse {
    @SerializedName("ID")
    public long ID;

    @SerializedName("Code")
    public String Code;

    @SerializedName("Name")
    public String Name;

    @SerializedName("Remarks")
    public String Remarks;
}
