package com.dcode.pricelabel.data;

import android.database.Cursor;
import androidx.room.EntityInsertionAdapter;
import androidx.room.RoomDatabase;
import androidx.room.RoomSQLiteQuery;
import androidx.room.SharedSQLiteStatement;
import androidx.room.util.CursorUtil;
import androidx.room.util.DBUtil;
import androidx.sqlite.db.SupportSQLiteQuery;
import androidx.sqlite.db.SupportSQLiteStatement;
import com.dcode.pricelabel.data.model.BFO_ITEM;
import com.dcode.pricelabel.data.model.BFO_PRICE_CHECK;
import com.dcode.pricelabel.data.model.BINS;
import com.dcode.pricelabel.data.model.BIN_ITEMS;
import com.dcode.pricelabel.data.model.GENERIC_COUNT;
import com.dcode.pricelabel.data.model.ITEMS;
import com.dcode.pricelabel.data.model.MAX_VALUE;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings({"unchecked", "deprecation"})
public final class GenericDao_Impl implements GenericDao {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter<ITEMS> __insertionAdapterOfITEMS;

  private final EntityInsertionAdapter<BINS> __insertionAdapterOfBINS;

  private final EntityInsertionAdapter<BIN_ITEMS> __insertionAdapterOfBIN_ITEMS;

  private final EntityInsertionAdapter<BFO_ITEM> __insertionAdapterOfBFO_ITEM;

  private final EntityInsertionAdapter<BFO_PRICE_CHECK> __insertionAdapterOfBFO_PRICE_CHECK;

  private final SharedSQLiteStatement __preparedStmtOfDeleteAllBins;

  private final SharedSQLiteStatement __preparedStmtOfDeleteBin;

  private final SharedSQLiteStatement __preparedStmtOfDeleteAllByBin;

  private final SharedSQLiteStatement __preparedStmtOfDeleteBinItemByID;

  private final SharedSQLiteStatement __preparedStmtOfDeleteStockItemBFo;

  private final SharedSQLiteStatement __preparedStmtOfDeleteAllStockItemBFo;

  private final SharedSQLiteStatement __preparedStmtOfDeletePriceCheckItems;

  private final SharedSQLiteStatement __preparedStmtOfDeleteAllBFoPriceCheck;

  public GenericDao_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfITEMS = new EntityInsertionAdapter<ITEMS>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR REPLACE INTO `ITEMS` (`BARCODE`,`ITEM_CODE`,`SIZE`,`COLOUR`,`BRAND`,`LONGDESC`,`OLD_PRICE`,`NEW_PRICE`,`LOCAL_DESCR`) VALUES (?,?,?,?,?,?,?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, ITEMS value) {
        if (value.BARCODE == null) {
          stmt.bindNull(1);
        } else {
          stmt.bindString(1, value.BARCODE);
        }
        if (value.ITEM_CODE == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.ITEM_CODE);
        }
        if (value.SIZE == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.SIZE);
        }
        if (value.COLOUR == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.COLOUR);
        }
        if (value.BRAND == null) {
          stmt.bindNull(5);
        } else {
          stmt.bindString(5, value.BRAND);
        }
        if (value.LONGDESC == null) {
          stmt.bindNull(6);
        } else {
          stmt.bindString(6, value.LONGDESC);
        }
        stmt.bindDouble(7, value.OLD_PRICE);
        stmt.bindDouble(8, value.NEW_PRICE);
        if (value.LOCAL_DESCR == null) {
          stmt.bindNull(9);
        } else {
          stmt.bindString(9, value.LOCAL_DESCR);
        }
      }
    };
    this.__insertionAdapterOfBINS = new EntityInsertionAdapter<BINS>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR REPLACE INTO `BINS` (`PK_ID`,`BIN_NO`,`BIN_MODE`,`STORE_CODE`,`DEST_LOC`,`USER_NAME`) VALUES (nullif(?, 0),?,?,?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, BINS value) {
        stmt.bindLong(1, value.PK_ID);
        if (value.BIN_NO == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.BIN_NO);
        }
        stmt.bindLong(3, value.BIN_MODE);
        if (value.STORE_CODE == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.STORE_CODE);
        }
        if (value.DEST_LOC == null) {
          stmt.bindNull(5);
        } else {
          stmt.bindString(5, value.DEST_LOC);
        }
        if (value.USER_NAME == null) {
          stmt.bindNull(6);
        } else {
          stmt.bindString(6, value.USER_NAME);
        }
      }
    };
    this.__insertionAdapterOfBIN_ITEMS = new EntityInsertionAdapter<BIN_ITEMS>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR REPLACE INTO `BIN_ITEMS` (`PK_ID`,`ScanTime`,`QTY`,`MATCH_FLAG`,`BIN_ID`,`BARCODE`,`ITEM_NO`,`PRODUCT_CODE`,`BRAND`,`ARTICLE_NO`,`ITEM_DESCRIPTION`,`VARIANT_CODE`,`SIZE_DESCR`,`QUANTITY`,`ALT_ITM`,`CUSTOM_1`,`CUSTOM_2`) VALUES (nullif(?, 0),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, BIN_ITEMS value) {
        stmt.bindLong(1, value.PK_ID);
        if (value.ScanTime == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.ScanTime);
        }
        stmt.bindLong(3, value.QTY);
        if (value.MATCH_FLAG == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.MATCH_FLAG);
        }
        stmt.bindLong(5, value.BIN_ID);
        if (value.BARCODE == null) {
          stmt.bindNull(6);
        } else {
          stmt.bindString(6, value.BARCODE);
        }
        if (value.ITEM_NO == null) {
          stmt.bindNull(7);
        } else {
          stmt.bindString(7, value.ITEM_NO);
        }
        if (value.PRODUCT_CODE == null) {
          stmt.bindNull(8);
        } else {
          stmt.bindString(8, value.PRODUCT_CODE);
        }
        if (value.BRAND == null) {
          stmt.bindNull(9);
        } else {
          stmt.bindString(9, value.BRAND);
        }
        if (value.ARTICLE_NO == null) {
          stmt.bindNull(10);
        } else {
          stmt.bindString(10, value.ARTICLE_NO);
        }
        if (value.ITEM_DESCRIPTION == null) {
          stmt.bindNull(11);
        } else {
          stmt.bindString(11, value.ITEM_DESCRIPTION);
        }
        if (value.VARIANT_CODE == null) {
          stmt.bindNull(12);
        } else {
          stmt.bindString(12, value.VARIANT_CODE);
        }
        if (value.SIZE_DESCR == null) {
          stmt.bindNull(13);
        } else {
          stmt.bindString(13, value.SIZE_DESCR);
        }
        if (value.QUANTITY == null) {
          stmt.bindNull(14);
        } else {
          stmt.bindString(14, value.QUANTITY);
        }
        if (value.ALT_ITM == null) {
          stmt.bindNull(15);
        } else {
          stmt.bindString(15, value.ALT_ITM);
        }
        if (value.CUSTOM_1 == null) {
          stmt.bindNull(16);
        } else {
          stmt.bindString(16, value.CUSTOM_1);
        }
        if (value.CUSTOM_2 == null) {
          stmt.bindNull(17);
        } else {
          stmt.bindString(17, value.CUSTOM_2);
        }
      }
    };
    this.__insertionAdapterOfBFO_ITEM = new EntityInsertionAdapter<BFO_ITEM>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR REPLACE INTO `BFO_ITEM` (`SL_NO`,`BARCODE`,`ITEM_NO`,`ITEM_DESCRIPTION`,`SIZE_DESCR`,`QUANTITY`,`PRICE`,`IS_VALIDATE`,`SCALE`,`UOM`,`BRAND`,`CATEGOTY`,`COLOUR`,`SCAN_TIME`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, BFO_ITEM value) {
        stmt.bindLong(1, value.SL_NO);
        if (value.BARCODE == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.BARCODE);
        }
        if (value.ITEM_NO == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.ITEM_NO);
        }
        if (value.ITEM_DESCRIPTION == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.ITEM_DESCRIPTION);
        }
        if (value.SIZE_DESCR == null) {
          stmt.bindNull(5);
        } else {
          stmt.bindString(5, value.SIZE_DESCR);
        }
        stmt.bindLong(6, value.QUANTITY);
        stmt.bindDouble(7, value.PRICE);
        final int _tmp;
        _tmp = value.IS_VALIDATE ? 1 : 0;
        stmt.bindLong(8, _tmp);
        if (value.SCALE == null) {
          stmt.bindNull(9);
        } else {
          stmt.bindString(9, value.SCALE);
        }
        if (value.UOM == null) {
          stmt.bindNull(10);
        } else {
          stmt.bindString(10, value.UOM);
        }
        if (value.BRAND == null) {
          stmt.bindNull(11);
        } else {
          stmt.bindString(11, value.BRAND);
        }
        if (value.CATEGOTY == null) {
          stmt.bindNull(12);
        } else {
          stmt.bindString(12, value.CATEGOTY);
        }
        if (value.COLOUR == null) {
          stmt.bindNull(13);
        } else {
          stmt.bindString(13, value.COLOUR);
        }
        if (value.SCAN_TIME == null) {
          stmt.bindNull(14);
        } else {
          stmt.bindString(14, value.SCAN_TIME);
        }
      }
    };
    this.__insertionAdapterOfBFO_PRICE_CHECK = new EntityInsertionAdapter<BFO_PRICE_CHECK>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR REPLACE INTO `BFO_PRICE_CHECK` (`SL_NO`,`BARCODE`,`ITEM_NO`,`ITEM_DESCRIPTION`,`SIZE_DESCR`,`QUANTITY`,`PRICE`,`IS_VALIDATE`,`SCALE`,`UOM`,`BRAND`,`CATEGOTY`,`COLOUR`,`SCAN_TIME`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, BFO_PRICE_CHECK value) {
        stmt.bindLong(1, value.SL_NO);
        if (value.BARCODE == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.BARCODE);
        }
        if (value.ITEM_NO == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.ITEM_NO);
        }
        if (value.ITEM_DESCRIPTION == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.ITEM_DESCRIPTION);
        }
        if (value.SIZE_DESCR == null) {
          stmt.bindNull(5);
        } else {
          stmt.bindString(5, value.SIZE_DESCR);
        }
        stmt.bindLong(6, value.QUANTITY);
        stmt.bindDouble(7, value.PRICE);
        final int _tmp;
        _tmp = value.IS_VALIDATE ? 1 : 0;
        stmt.bindLong(8, _tmp);
        if (value.SCALE == null) {
          stmt.bindNull(9);
        } else {
          stmt.bindString(9, value.SCALE);
        }
        if (value.UOM == null) {
          stmt.bindNull(10);
        } else {
          stmt.bindString(10, value.UOM);
        }
        if (value.BRAND == null) {
          stmt.bindNull(11);
        } else {
          stmt.bindString(11, value.BRAND);
        }
        if (value.CATEGOTY == null) {
          stmt.bindNull(12);
        } else {
          stmt.bindString(12, value.CATEGOTY);
        }
        if (value.COLOUR == null) {
          stmt.bindNull(13);
        } else {
          stmt.bindString(13, value.COLOUR);
        }
        if (value.SCAN_TIME == null) {
          stmt.bindNull(14);
        } else {
          stmt.bindString(14, value.SCAN_TIME);
        }
      }
    };
    this.__preparedStmtOfDeleteAllBins = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "DELETE FROM main.BINS";
        return _query;
      }
    };
    this.__preparedStmtOfDeleteBin = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "DELETE FROM main.BINS WHERE PK_ID=?";
        return _query;
      }
    };
    this.__preparedStmtOfDeleteAllByBin = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "DELETE FROM main.BIN_ITEMS WHERE BIN_ID=?";
        return _query;
      }
    };
    this.__preparedStmtOfDeleteBinItemByID = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "DELETE FROM BIN_ITEMS WHERE PK_ID=?";
        return _query;
      }
    };
    this.__preparedStmtOfDeleteStockItemBFo = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "DELETE FROM BFO_ITEM WHERE SL_NO=?";
        return _query;
      }
    };
    this.__preparedStmtOfDeleteAllStockItemBFo = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "DELETE FROM BFO_ITEM ";
        return _query;
      }
    };
    this.__preparedStmtOfDeletePriceCheckItems = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "DELETE FROM BFO_PRICE_CHECK WHERE SL_NO=?";
        return _query;
      }
    };
    this.__preparedStmtOfDeleteAllBFoPriceCheck = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "DELETE FROM BFO_PRICE_CHECK ";
        return _query;
      }
    };
  }

  @Override
  public void insertItem(final ITEMS... items) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      __insertionAdapterOfITEMS.insert(items);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void insertBin(final BINS... bins) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      __insertionAdapterOfBINS.insert(bins);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void insertBin_Items(final BIN_ITEMS... bin_items) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      __insertionAdapterOfBIN_ITEMS.insert(bin_items);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void insertBFoItem(final BFO_ITEM... bfo_items) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      __insertionAdapterOfBFO_ITEM.insert(bfo_items);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void insertBFoPriceCheck(final BFO_PRICE_CHECK... bfo_price_checks) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      __insertionAdapterOfBFO_PRICE_CHECK.insert(bfo_price_checks);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void deleteAllBins() {
    __db.assertNotSuspendingTransaction();
    final SupportSQLiteStatement _stmt = __preparedStmtOfDeleteAllBins.acquire();
    __db.beginTransaction();
    try {
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfDeleteAllBins.release(_stmt);
    }
  }

  @Override
  public void deleteBin(final int pk_id) {
    __db.assertNotSuspendingTransaction();
    final SupportSQLiteStatement _stmt = __preparedStmtOfDeleteBin.acquire();
    int _argIndex = 1;
    _stmt.bindLong(_argIndex, pk_id);
    __db.beginTransaction();
    try {
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfDeleteBin.release(_stmt);
    }
  }

  @Override
  public void deleteAllByBin(final int pk_id) {
    __db.assertNotSuspendingTransaction();
    final SupportSQLiteStatement _stmt = __preparedStmtOfDeleteAllByBin.acquire();
    int _argIndex = 1;
    _stmt.bindLong(_argIndex, pk_id);
    __db.beginTransaction();
    try {
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfDeleteAllByBin.release(_stmt);
    }
  }

  @Override
  public void deleteBinItemByID(final long pk_id) {
    __db.assertNotSuspendingTransaction();
    final SupportSQLiteStatement _stmt = __preparedStmtOfDeleteBinItemByID.acquire();
    int _argIndex = 1;
    _stmt.bindLong(_argIndex, pk_id);
    __db.beginTransaction();
    try {
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfDeleteBinItemByID.release(_stmt);
    }
  }

  @Override
  public void deleteStockItemBFo(final long sl_no) {
    __db.assertNotSuspendingTransaction();
    final SupportSQLiteStatement _stmt = __preparedStmtOfDeleteStockItemBFo.acquire();
    int _argIndex = 1;
    _stmt.bindLong(_argIndex, sl_no);
    __db.beginTransaction();
    try {
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfDeleteStockItemBFo.release(_stmt);
    }
  }

  @Override
  public void deleteAllStockItemBFo() {
    __db.assertNotSuspendingTransaction();
    final SupportSQLiteStatement _stmt = __preparedStmtOfDeleteAllStockItemBFo.acquire();
    __db.beginTransaction();
    try {
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfDeleteAllStockItemBFo.release(_stmt);
    }
  }

  @Override
  public void deletePriceCheckItems(final long sl_no) {
    __db.assertNotSuspendingTransaction();
    final SupportSQLiteStatement _stmt = __preparedStmtOfDeletePriceCheckItems.acquire();
    int _argIndex = 1;
    _stmt.bindLong(_argIndex, sl_no);
    __db.beginTransaction();
    try {
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfDeletePriceCheckItems.release(_stmt);
    }
  }

  @Override
  public void deleteAllBFoPriceCheck() {
    __db.assertNotSuspendingTransaction();
    final SupportSQLiteStatement _stmt = __preparedStmtOfDeleteAllBFoPriceCheck.acquire();
    __db.beginTransaction();
    try {
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfDeleteAllBFoPriceCheck.release(_stmt);
    }
  }

  @Override
  public ITEMS getItemsByBarcode(final String barcode) {
    final String _sql = "SELECT * FROM ITEMS WHERE BARCODE=?";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    if (barcode == null) {
      _statement.bindNull(_argIndex);
    } else {
      _statement.bindString(_argIndex, barcode);
    }
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final ITEMS _result;
      if(_cursor.moveToFirst()) {
        _result = __entityCursorConverter_comDcodePricelabelDataModelITEMS(_cursor);
      } else {
        _result = null;
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public BINS getBinByID(final int pk_id) {
    final String _sql = "SELECT * FROM BINS WHERE PK_ID = ?";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    _statement.bindLong(_argIndex, pk_id);
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfPKID = CursorUtil.getColumnIndexOrThrow(_cursor, "PK_ID");
      final int _cursorIndexOfBINNO = CursorUtil.getColumnIndexOrThrow(_cursor, "BIN_NO");
      final int _cursorIndexOfBINMODE = CursorUtil.getColumnIndexOrThrow(_cursor, "BIN_MODE");
      final int _cursorIndexOfSTORECODE = CursorUtil.getColumnIndexOrThrow(_cursor, "STORE_CODE");
      final int _cursorIndexOfDESTLOC = CursorUtil.getColumnIndexOrThrow(_cursor, "DEST_LOC");
      final int _cursorIndexOfUSERNAME = CursorUtil.getColumnIndexOrThrow(_cursor, "USER_NAME");
      final BINS _result;
      if(_cursor.moveToFirst()) {
        _result = new BINS();
        _result.PK_ID = _cursor.getInt(_cursorIndexOfPKID);
        _result.BIN_NO = _cursor.getString(_cursorIndexOfBINNO);
        _result.BIN_MODE = _cursor.getInt(_cursorIndexOfBINMODE);
        _result.STORE_CODE = _cursor.getString(_cursorIndexOfSTORECODE);
        _result.DEST_LOC = _cursor.getString(_cursorIndexOfDESTLOC);
        _result.USER_NAME = _cursor.getString(_cursorIndexOfUSERNAME);
      } else {
        _result = null;
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public BINS getBinByBinNoMode1(final String bin_no, final String user_name) {
    final String _sql = "SELECT * FROM main.BINS WHERE BIN_NO=? AND BIN_MODE = 1 AND USER_NAME=?";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 2);
    int _argIndex = 1;
    if (bin_no == null) {
      _statement.bindNull(_argIndex);
    } else {
      _statement.bindString(_argIndex, bin_no);
    }
    _argIndex = 2;
    if (user_name == null) {
      _statement.bindNull(_argIndex);
    } else {
      _statement.bindString(_argIndex, user_name);
    }
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfPKID = CursorUtil.getColumnIndexOrThrow(_cursor, "PK_ID");
      final int _cursorIndexOfBINNO = CursorUtil.getColumnIndexOrThrow(_cursor, "BIN_NO");
      final int _cursorIndexOfBINMODE = CursorUtil.getColumnIndexOrThrow(_cursor, "BIN_MODE");
      final int _cursorIndexOfSTORECODE = CursorUtil.getColumnIndexOrThrow(_cursor, "STORE_CODE");
      final int _cursorIndexOfDESTLOC = CursorUtil.getColumnIndexOrThrow(_cursor, "DEST_LOC");
      final int _cursorIndexOfUSERNAME = CursorUtil.getColumnIndexOrThrow(_cursor, "USER_NAME");
      final BINS _result;
      if(_cursor.moveToFirst()) {
        _result = new BINS();
        _result.PK_ID = _cursor.getInt(_cursorIndexOfPKID);
        _result.BIN_NO = _cursor.getString(_cursorIndexOfBINNO);
        _result.BIN_MODE = _cursor.getInt(_cursorIndexOfBINMODE);
        _result.STORE_CODE = _cursor.getString(_cursorIndexOfSTORECODE);
        _result.DEST_LOC = _cursor.getString(_cursorIndexOfDESTLOC);
        _result.USER_NAME = _cursor.getString(_cursorIndexOfUSERNAME);
      } else {
        _result = null;
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public BINS getBinByBinNoMode2(final String bin_no, final String destLoc,
      final String user_name) {
    final String _sql = "SELECT * FROM main.BINS WHERE BIN_NO=? AND BIN_MODE = 2 AND DEST_LOC = ? AND USER_NAME=?";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 3);
    int _argIndex = 1;
    if (bin_no == null) {
      _statement.bindNull(_argIndex);
    } else {
      _statement.bindString(_argIndex, bin_no);
    }
    _argIndex = 2;
    if (destLoc == null) {
      _statement.bindNull(_argIndex);
    } else {
      _statement.bindString(_argIndex, destLoc);
    }
    _argIndex = 3;
    if (user_name == null) {
      _statement.bindNull(_argIndex);
    } else {
      _statement.bindString(_argIndex, user_name);
    }
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfPKID = CursorUtil.getColumnIndexOrThrow(_cursor, "PK_ID");
      final int _cursorIndexOfBINNO = CursorUtil.getColumnIndexOrThrow(_cursor, "BIN_NO");
      final int _cursorIndexOfBINMODE = CursorUtil.getColumnIndexOrThrow(_cursor, "BIN_MODE");
      final int _cursorIndexOfSTORECODE = CursorUtil.getColumnIndexOrThrow(_cursor, "STORE_CODE");
      final int _cursorIndexOfDESTLOC = CursorUtil.getColumnIndexOrThrow(_cursor, "DEST_LOC");
      final int _cursorIndexOfUSERNAME = CursorUtil.getColumnIndexOrThrow(_cursor, "USER_NAME");
      final BINS _result;
      if(_cursor.moveToFirst()) {
        _result = new BINS();
        _result.PK_ID = _cursor.getInt(_cursorIndexOfPKID);
        _result.BIN_NO = _cursor.getString(_cursorIndexOfBINNO);
        _result.BIN_MODE = _cursor.getInt(_cursorIndexOfBINMODE);
        _result.STORE_CODE = _cursor.getString(_cursorIndexOfSTORECODE);
        _result.DEST_LOC = _cursor.getString(_cursorIndexOfDESTLOC);
        _result.USER_NAME = _cursor.getString(_cursorIndexOfUSERNAME);
      } else {
        _result = null;
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public List<BIN_ITEMS> getItemsByBin(final int bin_id) {
    final String _sql = "SELECT * FROM BIN_ITEMS WHERE BIN_ID=? ";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    _statement.bindLong(_argIndex, bin_id);
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfPKID = CursorUtil.getColumnIndexOrThrow(_cursor, "PK_ID");
      final int _cursorIndexOfScanTime = CursorUtil.getColumnIndexOrThrow(_cursor, "ScanTime");
      final int _cursorIndexOfQTY = CursorUtil.getColumnIndexOrThrow(_cursor, "QTY");
      final int _cursorIndexOfMATCHFLAG = CursorUtil.getColumnIndexOrThrow(_cursor, "MATCH_FLAG");
      final int _cursorIndexOfBINID = CursorUtil.getColumnIndexOrThrow(_cursor, "BIN_ID");
      final int _cursorIndexOfBARCODE = CursorUtil.getColumnIndexOrThrow(_cursor, "BARCODE");
      final int _cursorIndexOfITEMNO = CursorUtil.getColumnIndexOrThrow(_cursor, "ITEM_NO");
      final int _cursorIndexOfPRODUCTCODE = CursorUtil.getColumnIndexOrThrow(_cursor, "PRODUCT_CODE");
      final int _cursorIndexOfBRAND = CursorUtil.getColumnIndexOrThrow(_cursor, "BRAND");
      final int _cursorIndexOfARTICLENO = CursorUtil.getColumnIndexOrThrow(_cursor, "ARTICLE_NO");
      final int _cursorIndexOfITEMDESCRIPTION = CursorUtil.getColumnIndexOrThrow(_cursor, "ITEM_DESCRIPTION");
      final int _cursorIndexOfVARIANTCODE = CursorUtil.getColumnIndexOrThrow(_cursor, "VARIANT_CODE");
      final int _cursorIndexOfSIZEDESCR = CursorUtil.getColumnIndexOrThrow(_cursor, "SIZE_DESCR");
      final int _cursorIndexOfQUANTITY = CursorUtil.getColumnIndexOrThrow(_cursor, "QUANTITY");
      final int _cursorIndexOfALTITM = CursorUtil.getColumnIndexOrThrow(_cursor, "ALT_ITM");
      final int _cursorIndexOfCUSTOM1 = CursorUtil.getColumnIndexOrThrow(_cursor, "CUSTOM_1");
      final int _cursorIndexOfCUSTOM2 = CursorUtil.getColumnIndexOrThrow(_cursor, "CUSTOM_2");
      final List<BIN_ITEMS> _result = new ArrayList<BIN_ITEMS>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final BIN_ITEMS _item;
        _item = new BIN_ITEMS();
        _item.PK_ID = _cursor.getInt(_cursorIndexOfPKID);
        _item.ScanTime = _cursor.getString(_cursorIndexOfScanTime);
        _item.QTY = _cursor.getLong(_cursorIndexOfQTY);
        _item.MATCH_FLAG = _cursor.getString(_cursorIndexOfMATCHFLAG);
        _item.BIN_ID = _cursor.getInt(_cursorIndexOfBINID);
        _item.BARCODE = _cursor.getString(_cursorIndexOfBARCODE);
        _item.ITEM_NO = _cursor.getString(_cursorIndexOfITEMNO);
        _item.PRODUCT_CODE = _cursor.getString(_cursorIndexOfPRODUCTCODE);
        _item.BRAND = _cursor.getString(_cursorIndexOfBRAND);
        _item.ARTICLE_NO = _cursor.getString(_cursorIndexOfARTICLENO);
        _item.ITEM_DESCRIPTION = _cursor.getString(_cursorIndexOfITEMDESCRIPTION);
        _item.VARIANT_CODE = _cursor.getString(_cursorIndexOfVARIANTCODE);
        _item.SIZE_DESCR = _cursor.getString(_cursorIndexOfSIZEDESCR);
        _item.QUANTITY = _cursor.getString(_cursorIndexOfQUANTITY);
        _item.ALT_ITM = _cursor.getString(_cursorIndexOfALTITM);
        _item.CUSTOM_1 = _cursor.getString(_cursorIndexOfCUSTOM1);
        _item.CUSTOM_2 = _cursor.getString(_cursorIndexOfCUSTOM2);
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public List<BFO_ITEM> getStockTakeItems() {
    final String _sql = "SELECT * FROM BFO_ITEM";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfSLNO = CursorUtil.getColumnIndexOrThrow(_cursor, "SL_NO");
      final int _cursorIndexOfBARCODE = CursorUtil.getColumnIndexOrThrow(_cursor, "BARCODE");
      final int _cursorIndexOfITEMNO = CursorUtil.getColumnIndexOrThrow(_cursor, "ITEM_NO");
      final int _cursorIndexOfITEMDESCRIPTION = CursorUtil.getColumnIndexOrThrow(_cursor, "ITEM_DESCRIPTION");
      final int _cursorIndexOfSIZEDESCR = CursorUtil.getColumnIndexOrThrow(_cursor, "SIZE_DESCR");
      final int _cursorIndexOfQUANTITY = CursorUtil.getColumnIndexOrThrow(_cursor, "QUANTITY");
      final int _cursorIndexOfPRICE = CursorUtil.getColumnIndexOrThrow(_cursor, "PRICE");
      final int _cursorIndexOfISVALIDATE = CursorUtil.getColumnIndexOrThrow(_cursor, "IS_VALIDATE");
      final int _cursorIndexOfSCALE = CursorUtil.getColumnIndexOrThrow(_cursor, "SCALE");
      final int _cursorIndexOfUOM = CursorUtil.getColumnIndexOrThrow(_cursor, "UOM");
      final int _cursorIndexOfBRAND = CursorUtil.getColumnIndexOrThrow(_cursor, "BRAND");
      final int _cursorIndexOfCATEGOTY = CursorUtil.getColumnIndexOrThrow(_cursor, "CATEGOTY");
      final int _cursorIndexOfCOLOUR = CursorUtil.getColumnIndexOrThrow(_cursor, "COLOUR");
      final int _cursorIndexOfSCANTIME = CursorUtil.getColumnIndexOrThrow(_cursor, "SCAN_TIME");
      final List<BFO_ITEM> _result = new ArrayList<BFO_ITEM>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final BFO_ITEM _item;
        _item = new BFO_ITEM();
        _item.SL_NO = _cursor.getLong(_cursorIndexOfSLNO);
        _item.BARCODE = _cursor.getString(_cursorIndexOfBARCODE);
        _item.ITEM_NO = _cursor.getString(_cursorIndexOfITEMNO);
        _item.ITEM_DESCRIPTION = _cursor.getString(_cursorIndexOfITEMDESCRIPTION);
        _item.SIZE_DESCR = _cursor.getString(_cursorIndexOfSIZEDESCR);
        _item.QUANTITY = _cursor.getInt(_cursorIndexOfQUANTITY);
        _item.PRICE = _cursor.getDouble(_cursorIndexOfPRICE);
        final int _tmp;
        _tmp = _cursor.getInt(_cursorIndexOfISVALIDATE);
        _item.IS_VALIDATE = _tmp != 0;
        _item.SCALE = _cursor.getString(_cursorIndexOfSCALE);
        _item.UOM = _cursor.getString(_cursorIndexOfUOM);
        _item.BRAND = _cursor.getString(_cursorIndexOfBRAND);
        _item.CATEGOTY = _cursor.getString(_cursorIndexOfCATEGOTY);
        _item.COLOUR = _cursor.getString(_cursorIndexOfCOLOUR);
        _item.SCAN_TIME = _cursor.getString(_cursorIndexOfSCANTIME);
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public List<BFO_PRICE_CHECK> getPriceCheckItems() {
    final String _sql = "SELECT * FROM BFO_PRICE_CHECK";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfSLNO = CursorUtil.getColumnIndexOrThrow(_cursor, "SL_NO");
      final int _cursorIndexOfBARCODE = CursorUtil.getColumnIndexOrThrow(_cursor, "BARCODE");
      final int _cursorIndexOfITEMNO = CursorUtil.getColumnIndexOrThrow(_cursor, "ITEM_NO");
      final int _cursorIndexOfITEMDESCRIPTION = CursorUtil.getColumnIndexOrThrow(_cursor, "ITEM_DESCRIPTION");
      final int _cursorIndexOfSIZEDESCR = CursorUtil.getColumnIndexOrThrow(_cursor, "SIZE_DESCR");
      final int _cursorIndexOfQUANTITY = CursorUtil.getColumnIndexOrThrow(_cursor, "QUANTITY");
      final int _cursorIndexOfPRICE = CursorUtil.getColumnIndexOrThrow(_cursor, "PRICE");
      final int _cursorIndexOfISVALIDATE = CursorUtil.getColumnIndexOrThrow(_cursor, "IS_VALIDATE");
      final int _cursorIndexOfSCALE = CursorUtil.getColumnIndexOrThrow(_cursor, "SCALE");
      final int _cursorIndexOfUOM = CursorUtil.getColumnIndexOrThrow(_cursor, "UOM");
      final int _cursorIndexOfBRAND = CursorUtil.getColumnIndexOrThrow(_cursor, "BRAND");
      final int _cursorIndexOfCATEGOTY = CursorUtil.getColumnIndexOrThrow(_cursor, "CATEGOTY");
      final int _cursorIndexOfCOLOUR = CursorUtil.getColumnIndexOrThrow(_cursor, "COLOUR");
      final int _cursorIndexOfSCANTIME = CursorUtil.getColumnIndexOrThrow(_cursor, "SCAN_TIME");
      final List<BFO_PRICE_CHECK> _result = new ArrayList<BFO_PRICE_CHECK>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final BFO_PRICE_CHECK _item;
        _item = new BFO_PRICE_CHECK();
        _item.SL_NO = _cursor.getLong(_cursorIndexOfSLNO);
        _item.BARCODE = _cursor.getString(_cursorIndexOfBARCODE);
        _item.ITEM_NO = _cursor.getString(_cursorIndexOfITEMNO);
        _item.ITEM_DESCRIPTION = _cursor.getString(_cursorIndexOfITEMDESCRIPTION);
        _item.SIZE_DESCR = _cursor.getString(_cursorIndexOfSIZEDESCR);
        _item.QUANTITY = _cursor.getInt(_cursorIndexOfQUANTITY);
        _item.PRICE = _cursor.getDouble(_cursorIndexOfPRICE);
        final int _tmp;
        _tmp = _cursor.getInt(_cursorIndexOfISVALIDATE);
        _item.IS_VALIDATE = _tmp != 0;
        _item.SCALE = _cursor.getString(_cursorIndexOfSCALE);
        _item.UOM = _cursor.getString(_cursorIndexOfUOM);
        _item.BRAND = _cursor.getString(_cursorIndexOfBRAND);
        _item.CATEGOTY = _cursor.getString(_cursorIndexOfCATEGOTY);
        _item.COLOUR = _cursor.getString(_cursorIndexOfCOLOUR);
        _item.SCAN_TIME = _cursor.getString(_cursorIndexOfSCANTIME);
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public List<BFO_ITEM> getPriceCheckItemExport() {
    final String _sql = "SELECT * FROM BFO_PRICE_CHECK";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfSLNO = CursorUtil.getColumnIndexOrThrow(_cursor, "SL_NO");
      final int _cursorIndexOfBARCODE = CursorUtil.getColumnIndexOrThrow(_cursor, "BARCODE");
      final int _cursorIndexOfITEMNO = CursorUtil.getColumnIndexOrThrow(_cursor, "ITEM_NO");
      final int _cursorIndexOfITEMDESCRIPTION = CursorUtil.getColumnIndexOrThrow(_cursor, "ITEM_DESCRIPTION");
      final int _cursorIndexOfSIZEDESCR = CursorUtil.getColumnIndexOrThrow(_cursor, "SIZE_DESCR");
      final int _cursorIndexOfQUANTITY = CursorUtil.getColumnIndexOrThrow(_cursor, "QUANTITY");
      final int _cursorIndexOfPRICE = CursorUtil.getColumnIndexOrThrow(_cursor, "PRICE");
      final int _cursorIndexOfISVALIDATE = CursorUtil.getColumnIndexOrThrow(_cursor, "IS_VALIDATE");
      final int _cursorIndexOfSCALE = CursorUtil.getColumnIndexOrThrow(_cursor, "SCALE");
      final int _cursorIndexOfUOM = CursorUtil.getColumnIndexOrThrow(_cursor, "UOM");
      final int _cursorIndexOfBRAND = CursorUtil.getColumnIndexOrThrow(_cursor, "BRAND");
      final int _cursorIndexOfCATEGOTY = CursorUtil.getColumnIndexOrThrow(_cursor, "CATEGOTY");
      final int _cursorIndexOfCOLOUR = CursorUtil.getColumnIndexOrThrow(_cursor, "COLOUR");
      final int _cursorIndexOfSCANTIME = CursorUtil.getColumnIndexOrThrow(_cursor, "SCAN_TIME");
      final List<BFO_ITEM> _result = new ArrayList<BFO_ITEM>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final BFO_ITEM _item;
        _item = new BFO_ITEM();
        _item.SL_NO = _cursor.getLong(_cursorIndexOfSLNO);
        _item.BARCODE = _cursor.getString(_cursorIndexOfBARCODE);
        _item.ITEM_NO = _cursor.getString(_cursorIndexOfITEMNO);
        _item.ITEM_DESCRIPTION = _cursor.getString(_cursorIndexOfITEMDESCRIPTION);
        _item.SIZE_DESCR = _cursor.getString(_cursorIndexOfSIZEDESCR);
        _item.QUANTITY = _cursor.getInt(_cursorIndexOfQUANTITY);
        _item.PRICE = _cursor.getDouble(_cursorIndexOfPRICE);
        final int _tmp;
        _tmp = _cursor.getInt(_cursorIndexOfISVALIDATE);
        _item.IS_VALIDATE = _tmp != 0;
        _item.SCALE = _cursor.getString(_cursorIndexOfSCALE);
        _item.UOM = _cursor.getString(_cursorIndexOfUOM);
        _item.BRAND = _cursor.getString(_cursorIndexOfBRAND);
        _item.CATEGOTY = _cursor.getString(_cursorIndexOfCATEGOTY);
        _item.COLOUR = _cursor.getString(_cursorIndexOfCOLOUR);
        _item.SCAN_TIME = _cursor.getString(_cursorIndexOfSCANTIME);
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public List<BFO_ITEM> getStockSummary() {
    final String _sql = "SELECT 0 SL_NO,BARCODE,ITEM_DESCRIPTION,SUM(QUANTITY) AS QUANTITY,NULL AS ITEM_NO,PRICE,NULL AS IS_VALIDATE,SIZE_DESCR  FROM BFO_ITEM GROUP BY BARCODE,ITEM_DESCRIPTION,SIZE_DESCR,PRICE";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfSLNO = CursorUtil.getColumnIndexOrThrow(_cursor, "SL_NO");
      final int _cursorIndexOfBARCODE = CursorUtil.getColumnIndexOrThrow(_cursor, "BARCODE");
      final int _cursorIndexOfITEMDESCRIPTION = CursorUtil.getColumnIndexOrThrow(_cursor, "ITEM_DESCRIPTION");
      final int _cursorIndexOfQUANTITY = CursorUtil.getColumnIndexOrThrow(_cursor, "QUANTITY");
      final int _cursorIndexOfITEMNO = CursorUtil.getColumnIndexOrThrow(_cursor, "ITEM_NO");
      final int _cursorIndexOfPRICE = CursorUtil.getColumnIndexOrThrow(_cursor, "PRICE");
      final int _cursorIndexOfISVALIDATE = CursorUtil.getColumnIndexOrThrow(_cursor, "IS_VALIDATE");
      final int _cursorIndexOfSIZEDESCR = CursorUtil.getColumnIndexOrThrow(_cursor, "SIZE_DESCR");
      final List<BFO_ITEM> _result = new ArrayList<BFO_ITEM>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final BFO_ITEM _item;
        _item = new BFO_ITEM();
        _item.SL_NO = _cursor.getLong(_cursorIndexOfSLNO);
        _item.BARCODE = _cursor.getString(_cursorIndexOfBARCODE);
        _item.ITEM_DESCRIPTION = _cursor.getString(_cursorIndexOfITEMDESCRIPTION);
        _item.QUANTITY = _cursor.getInt(_cursorIndexOfQUANTITY);
        _item.ITEM_NO = _cursor.getString(_cursorIndexOfITEMNO);
        _item.PRICE = _cursor.getDouble(_cursorIndexOfPRICE);
        final int _tmp;
        _tmp = _cursor.getInt(_cursorIndexOfISVALIDATE);
        _item.IS_VALIDATE = _tmp != 0;
        _item.SIZE_DESCR = _cursor.getString(_cursorIndexOfSIZEDESCR);
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public MAX_VALUE getMaxValue(final SupportSQLiteQuery query) {
    final SupportSQLiteQuery _internalQuery = query;
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _internalQuery, false, null);
    try {
      final int _cursorIndexOfMAXVAL = CursorUtil.getColumnIndex(_cursor, "MAX_VAL");
      final MAX_VALUE _result;
      if(_cursor.moveToFirst()) {
        _result = new MAX_VALUE();
        if (_cursorIndexOfMAXVAL != -1) {
          _result.MAX_VAL = _cursor.getLong(_cursorIndexOfMAXVAL);
        }
      } else {
        _result = null;
      }
      return _result;
    } finally {
      _cursor.close();
    }
  }

  @Override
  public GENERIC_COUNT getCount(final SupportSQLiteQuery query) {
    final SupportSQLiteQuery _internalQuery = query;
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _internalQuery, false, null);
    try {
      final int _cursorIndexOfTOTALCOUNT = CursorUtil.getColumnIndex(_cursor, "TOTAL_COUNT");
      final GENERIC_COUNT _result;
      if(_cursor.moveToFirst()) {
        _result = new GENERIC_COUNT();
        if (_cursorIndexOfTOTALCOUNT != -1) {
          _result.TOTAL_COUNT = _cursor.getInt(_cursorIndexOfTOTALCOUNT);
        }
      } else {
        _result = null;
      }
      return _result;
    } finally {
      _cursor.close();
    }
  }

  private ITEMS __entityCursorConverter_comDcodePricelabelDataModelITEMS(Cursor cursor) {
    final ITEMS _entity;
    final int _cursorIndexOfBARCODE = cursor.getColumnIndex("BARCODE");
    final int _cursorIndexOfITEMCODE = cursor.getColumnIndex("ITEM_CODE");
    final int _cursorIndexOfSIZE = cursor.getColumnIndex("SIZE");
    final int _cursorIndexOfCOLOUR = cursor.getColumnIndex("COLOUR");
    final int _cursorIndexOfBRAND = cursor.getColumnIndex("BRAND");
    final int _cursorIndexOfLONGDESC = cursor.getColumnIndex("LONGDESC");
    final int _cursorIndexOfOLDPRICE = cursor.getColumnIndex("OLD_PRICE");
    final int _cursorIndexOfNEWPRICE = cursor.getColumnIndex("NEW_PRICE");
    final int _cursorIndexOfLOCALDESCR = cursor.getColumnIndex("LOCAL_DESCR");
    _entity = new ITEMS();
    if (_cursorIndexOfBARCODE != -1) {
      _entity.BARCODE = cursor.getString(_cursorIndexOfBARCODE);
    }
    if (_cursorIndexOfITEMCODE != -1) {
      _entity.ITEM_CODE = cursor.getString(_cursorIndexOfITEMCODE);
    }
    if (_cursorIndexOfSIZE != -1) {
      _entity.SIZE = cursor.getString(_cursorIndexOfSIZE);
    }
    if (_cursorIndexOfCOLOUR != -1) {
      _entity.COLOUR = cursor.getString(_cursorIndexOfCOLOUR);
    }
    if (_cursorIndexOfBRAND != -1) {
      _entity.BRAND = cursor.getString(_cursorIndexOfBRAND);
    }
    if (_cursorIndexOfLONGDESC != -1) {
      _entity.LONGDESC = cursor.getString(_cursorIndexOfLONGDESC);
    }
    if (_cursorIndexOfOLDPRICE != -1) {
      _entity.OLD_PRICE = cursor.getFloat(_cursorIndexOfOLDPRICE);
    }
    if (_cursorIndexOfNEWPRICE != -1) {
      _entity.NEW_PRICE = cursor.getFloat(_cursorIndexOfNEWPRICE);
    }
    if (_cursorIndexOfLOCALDESCR != -1) {
      _entity.LOCAL_DESCR = cursor.getString(_cursorIndexOfLOCALDESCR);
    }
    return _entity;
  }
}
