package com.dcode.pricelabel.data;

import androidx.room.DatabaseConfiguration;
import androidx.room.InvalidationTracker;
import androidx.room.RoomOpenHelper;
import androidx.room.RoomOpenHelper.Delegate;
import androidx.room.RoomOpenHelper.ValidationResult;
import androidx.room.util.DBUtil;
import androidx.room.util.TableInfo;
import androidx.room.util.TableInfo.Column;
import androidx.room.util.TableInfo.ForeignKey;
import androidx.room.util.TableInfo.Index;
import androidx.sqlite.db.SupportSQLiteDatabase;
import androidx.sqlite.db.SupportSQLiteOpenHelper;
import androidx.sqlite.db.SupportSQLiteOpenHelper.Callback;
import androidx.sqlite.db.SupportSQLiteOpenHelper.Configuration;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

@SuppressWarnings({"unchecked", "deprecation"})
public final class AppDatabase_Impl extends AppDatabase {
  private volatile GenericDao _genericDao;

  @Override
  protected SupportSQLiteOpenHelper createOpenHelper(DatabaseConfiguration configuration) {
    final SupportSQLiteOpenHelper.Callback _openCallback = new RoomOpenHelper(configuration, new RoomOpenHelper.Delegate(1) {
      @Override
      public void createAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("CREATE TABLE IF NOT EXISTS `BINS` (`PK_ID` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `BIN_NO` TEXT, `BIN_MODE` INTEGER NOT NULL, `STORE_CODE` TEXT, `DEST_LOC` TEXT DEFAULT '', `USER_NAME` TEXT)");
        _db.execSQL("CREATE TABLE IF NOT EXISTS `BIN_ITEMS` (`PK_ID` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `ScanTime` TEXT, `QTY` INTEGER NOT NULL, `MATCH_FLAG` TEXT, `BIN_ID` INTEGER NOT NULL, `BARCODE` TEXT, `ITEM_NO` TEXT, `PRODUCT_CODE` TEXT, `BRAND` TEXT, `ARTICLE_NO` TEXT, `ITEM_DESCRIPTION` TEXT, `VARIANT_CODE` TEXT, `SIZE_DESCR` TEXT, `QUANTITY` TEXT, `ALT_ITM` TEXT, `CUSTOM_1` TEXT, `CUSTOM_2` TEXT)");
        _db.execSQL("CREATE TABLE IF NOT EXISTS `ITEMS` (`BARCODE` TEXT NOT NULL, `ITEM_CODE` TEXT, `SIZE` TEXT, `COLOUR` TEXT, `BRAND` TEXT, `LONGDESC` TEXT, `OLD_PRICE` REAL NOT NULL, `NEW_PRICE` REAL NOT NULL, `LOCAL_DESCR` TEXT, PRIMARY KEY(`BARCODE`))");
        _db.execSQL("CREATE TABLE IF NOT EXISTS `BFO_ITEM` (`SL_NO` INTEGER NOT NULL, `BARCODE` TEXT NOT NULL, `ITEM_NO` TEXT NOT NULL, `ITEM_DESCRIPTION` TEXT, `SIZE_DESCR` TEXT, `QUANTITY` INTEGER NOT NULL, `PRICE` REAL NOT NULL, `IS_VALIDATE` INTEGER NOT NULL, `SCALE` TEXT, `UOM` TEXT, `BRAND` TEXT, `CATEGOTY` TEXT, `COLOUR` TEXT, `SCAN_TIME` TEXT, PRIMARY KEY(`SL_NO`))");
        _db.execSQL("CREATE TABLE IF NOT EXISTS `BFO_PRICE_CHECK` (`SL_NO` INTEGER NOT NULL, `BARCODE` TEXT NOT NULL, `ITEM_NO` TEXT NOT NULL, `ITEM_DESCRIPTION` TEXT, `SIZE_DESCR` TEXT, `QUANTITY` INTEGER NOT NULL, `PRICE` REAL NOT NULL, `IS_VALIDATE` INTEGER NOT NULL, `SCALE` TEXT, `UOM` TEXT, `BRAND` TEXT, `CATEGOTY` TEXT, `COLOUR` TEXT, `SCAN_TIME` TEXT, PRIMARY KEY(`SL_NO`))");
        _db.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
        _db.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, 'f16406185e0ff082942ba893fad5c8cc')");
      }

      @Override
      public void dropAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("DROP TABLE IF EXISTS `BINS`");
        _db.execSQL("DROP TABLE IF EXISTS `BIN_ITEMS`");
        _db.execSQL("DROP TABLE IF EXISTS `ITEMS`");
        _db.execSQL("DROP TABLE IF EXISTS `BFO_ITEM`");
        _db.execSQL("DROP TABLE IF EXISTS `BFO_PRICE_CHECK`");
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onDestructiveMigration(_db);
          }
        }
      }

      @Override
      protected void onCreate(SupportSQLiteDatabase _db) {
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onCreate(_db);
          }
        }
      }

      @Override
      public void onOpen(SupportSQLiteDatabase _db) {
        mDatabase = _db;
        internalInitInvalidationTracker(_db);
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onOpen(_db);
          }
        }
      }

      @Override
      public void onPreMigrate(SupportSQLiteDatabase _db) {
        DBUtil.dropFtsSyncTriggers(_db);
      }

      @Override
      public void onPostMigrate(SupportSQLiteDatabase _db) {
      }

      @Override
      protected RoomOpenHelper.ValidationResult onValidateSchema(SupportSQLiteDatabase _db) {
        final HashMap<String, TableInfo.Column> _columnsBINS = new HashMap<String, TableInfo.Column>(6);
        _columnsBINS.put("PK_ID", new TableInfo.Column("PK_ID", "INTEGER", true, 1, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsBINS.put("BIN_NO", new TableInfo.Column("BIN_NO", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsBINS.put("BIN_MODE", new TableInfo.Column("BIN_MODE", "INTEGER", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsBINS.put("STORE_CODE", new TableInfo.Column("STORE_CODE", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsBINS.put("DEST_LOC", new TableInfo.Column("DEST_LOC", "TEXT", false, 0, "''", TableInfo.CREATED_FROM_ENTITY));
        _columnsBINS.put("USER_NAME", new TableInfo.Column("USER_NAME", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        final HashSet<TableInfo.ForeignKey> _foreignKeysBINS = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesBINS = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoBINS = new TableInfo("BINS", _columnsBINS, _foreignKeysBINS, _indicesBINS);
        final TableInfo _existingBINS = TableInfo.read(_db, "BINS");
        if (! _infoBINS.equals(_existingBINS)) {
          return new RoomOpenHelper.ValidationResult(false, "BINS(com.dcode.pricelabel.data.model.BINS).\n"
                  + " Expected:\n" + _infoBINS + "\n"
                  + " Found:\n" + _existingBINS);
        }
        final HashMap<String, TableInfo.Column> _columnsBINITEMS = new HashMap<String, TableInfo.Column>(17);
        _columnsBINITEMS.put("PK_ID", new TableInfo.Column("PK_ID", "INTEGER", true, 1, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsBINITEMS.put("ScanTime", new TableInfo.Column("ScanTime", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsBINITEMS.put("QTY", new TableInfo.Column("QTY", "INTEGER", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsBINITEMS.put("MATCH_FLAG", new TableInfo.Column("MATCH_FLAG", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsBINITEMS.put("BIN_ID", new TableInfo.Column("BIN_ID", "INTEGER", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsBINITEMS.put("BARCODE", new TableInfo.Column("BARCODE", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsBINITEMS.put("ITEM_NO", new TableInfo.Column("ITEM_NO", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsBINITEMS.put("PRODUCT_CODE", new TableInfo.Column("PRODUCT_CODE", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsBINITEMS.put("BRAND", new TableInfo.Column("BRAND", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsBINITEMS.put("ARTICLE_NO", new TableInfo.Column("ARTICLE_NO", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsBINITEMS.put("ITEM_DESCRIPTION", new TableInfo.Column("ITEM_DESCRIPTION", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsBINITEMS.put("VARIANT_CODE", new TableInfo.Column("VARIANT_CODE", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsBINITEMS.put("SIZE_DESCR", new TableInfo.Column("SIZE_DESCR", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsBINITEMS.put("QUANTITY", new TableInfo.Column("QUANTITY", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsBINITEMS.put("ALT_ITM", new TableInfo.Column("ALT_ITM", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsBINITEMS.put("CUSTOM_1", new TableInfo.Column("CUSTOM_1", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsBINITEMS.put("CUSTOM_2", new TableInfo.Column("CUSTOM_2", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        final HashSet<TableInfo.ForeignKey> _foreignKeysBINITEMS = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesBINITEMS = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoBINITEMS = new TableInfo("BIN_ITEMS", _columnsBINITEMS, _foreignKeysBINITEMS, _indicesBINITEMS);
        final TableInfo _existingBINITEMS = TableInfo.read(_db, "BIN_ITEMS");
        if (! _infoBINITEMS.equals(_existingBINITEMS)) {
          return new RoomOpenHelper.ValidationResult(false, "BIN_ITEMS(com.dcode.pricelabel.data.model.BIN_ITEMS).\n"
                  + " Expected:\n" + _infoBINITEMS + "\n"
                  + " Found:\n" + _existingBINITEMS);
        }
        final HashMap<String, TableInfo.Column> _columnsITEMS = new HashMap<String, TableInfo.Column>(9);
        _columnsITEMS.put("BARCODE", new TableInfo.Column("BARCODE", "TEXT", true, 1, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsITEMS.put("ITEM_CODE", new TableInfo.Column("ITEM_CODE", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsITEMS.put("SIZE", new TableInfo.Column("SIZE", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsITEMS.put("COLOUR", new TableInfo.Column("COLOUR", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsITEMS.put("BRAND", new TableInfo.Column("BRAND", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsITEMS.put("LONGDESC", new TableInfo.Column("LONGDESC", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsITEMS.put("OLD_PRICE", new TableInfo.Column("OLD_PRICE", "REAL", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsITEMS.put("NEW_PRICE", new TableInfo.Column("NEW_PRICE", "REAL", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsITEMS.put("LOCAL_DESCR", new TableInfo.Column("LOCAL_DESCR", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        final HashSet<TableInfo.ForeignKey> _foreignKeysITEMS = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesITEMS = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoITEMS = new TableInfo("ITEMS", _columnsITEMS, _foreignKeysITEMS, _indicesITEMS);
        final TableInfo _existingITEMS = TableInfo.read(_db, "ITEMS");
        if (! _infoITEMS.equals(_existingITEMS)) {
          return new RoomOpenHelper.ValidationResult(false, "ITEMS(com.dcode.pricelabel.data.model.ITEMS).\n"
                  + " Expected:\n" + _infoITEMS + "\n"
                  + " Found:\n" + _existingITEMS);
        }
        final HashMap<String, TableInfo.Column> _columnsBFOITEM = new HashMap<String, TableInfo.Column>(14);
        _columnsBFOITEM.put("SL_NO", new TableInfo.Column("SL_NO", "INTEGER", true, 1, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsBFOITEM.put("BARCODE", new TableInfo.Column("BARCODE", "TEXT", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsBFOITEM.put("ITEM_NO", new TableInfo.Column("ITEM_NO", "TEXT", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsBFOITEM.put("ITEM_DESCRIPTION", new TableInfo.Column("ITEM_DESCRIPTION", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsBFOITEM.put("SIZE_DESCR", new TableInfo.Column("SIZE_DESCR", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsBFOITEM.put("QUANTITY", new TableInfo.Column("QUANTITY", "INTEGER", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsBFOITEM.put("PRICE", new TableInfo.Column("PRICE", "REAL", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsBFOITEM.put("IS_VALIDATE", new TableInfo.Column("IS_VALIDATE", "INTEGER", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsBFOITEM.put("SCALE", new TableInfo.Column("SCALE", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsBFOITEM.put("UOM", new TableInfo.Column("UOM", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsBFOITEM.put("BRAND", new TableInfo.Column("BRAND", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsBFOITEM.put("CATEGOTY", new TableInfo.Column("CATEGOTY", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsBFOITEM.put("COLOUR", new TableInfo.Column("COLOUR", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsBFOITEM.put("SCAN_TIME", new TableInfo.Column("SCAN_TIME", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        final HashSet<TableInfo.ForeignKey> _foreignKeysBFOITEM = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesBFOITEM = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoBFOITEM = new TableInfo("BFO_ITEM", _columnsBFOITEM, _foreignKeysBFOITEM, _indicesBFOITEM);
        final TableInfo _existingBFOITEM = TableInfo.read(_db, "BFO_ITEM");
        if (! _infoBFOITEM.equals(_existingBFOITEM)) {
          return new RoomOpenHelper.ValidationResult(false, "BFO_ITEM(com.dcode.pricelabel.data.model.BFO_ITEM).\n"
                  + " Expected:\n" + _infoBFOITEM + "\n"
                  + " Found:\n" + _existingBFOITEM);
        }
        final HashMap<String, TableInfo.Column> _columnsBFOPRICECHECK = new HashMap<String, TableInfo.Column>(14);
        _columnsBFOPRICECHECK.put("SL_NO", new TableInfo.Column("SL_NO", "INTEGER", true, 1, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsBFOPRICECHECK.put("BARCODE", new TableInfo.Column("BARCODE", "TEXT", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsBFOPRICECHECK.put("ITEM_NO", new TableInfo.Column("ITEM_NO", "TEXT", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsBFOPRICECHECK.put("ITEM_DESCRIPTION", new TableInfo.Column("ITEM_DESCRIPTION", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsBFOPRICECHECK.put("SIZE_DESCR", new TableInfo.Column("SIZE_DESCR", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsBFOPRICECHECK.put("QUANTITY", new TableInfo.Column("QUANTITY", "INTEGER", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsBFOPRICECHECK.put("PRICE", new TableInfo.Column("PRICE", "REAL", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsBFOPRICECHECK.put("IS_VALIDATE", new TableInfo.Column("IS_VALIDATE", "INTEGER", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsBFOPRICECHECK.put("SCALE", new TableInfo.Column("SCALE", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsBFOPRICECHECK.put("UOM", new TableInfo.Column("UOM", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsBFOPRICECHECK.put("BRAND", new TableInfo.Column("BRAND", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsBFOPRICECHECK.put("CATEGOTY", new TableInfo.Column("CATEGOTY", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsBFOPRICECHECK.put("COLOUR", new TableInfo.Column("COLOUR", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsBFOPRICECHECK.put("SCAN_TIME", new TableInfo.Column("SCAN_TIME", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        final HashSet<TableInfo.ForeignKey> _foreignKeysBFOPRICECHECK = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesBFOPRICECHECK = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoBFOPRICECHECK = new TableInfo("BFO_PRICE_CHECK", _columnsBFOPRICECHECK, _foreignKeysBFOPRICECHECK, _indicesBFOPRICECHECK);
        final TableInfo _existingBFOPRICECHECK = TableInfo.read(_db, "BFO_PRICE_CHECK");
        if (! _infoBFOPRICECHECK.equals(_existingBFOPRICECHECK)) {
          return new RoomOpenHelper.ValidationResult(false, "BFO_PRICE_CHECK(com.dcode.pricelabel.data.model.BFO_PRICE_CHECK).\n"
                  + " Expected:\n" + _infoBFOPRICECHECK + "\n"
                  + " Found:\n" + _existingBFOPRICECHECK);
        }
        return new RoomOpenHelper.ValidationResult(true, null);
      }
    }, "f16406185e0ff082942ba893fad5c8cc", "3edb02075e1452c65c0f2cbd69977806");
    final SupportSQLiteOpenHelper.Configuration _sqliteConfig = SupportSQLiteOpenHelper.Configuration.builder(configuration.context)
        .name(configuration.name)
        .callback(_openCallback)
        .build();
    final SupportSQLiteOpenHelper _helper = configuration.sqliteOpenHelperFactory.create(_sqliteConfig);
    return _helper;
  }

  @Override
  protected InvalidationTracker createInvalidationTracker() {
    final HashMap<String, String> _shadowTablesMap = new HashMap<String, String>(0);
    HashMap<String, Set<String>> _viewTables = new HashMap<String, Set<String>>(0);
    return new InvalidationTracker(this, _shadowTablesMap, _viewTables, "BINS","BIN_ITEMS","ITEMS","BFO_ITEM","BFO_PRICE_CHECK");
  }

  @Override
  public void clearAllTables() {
    super.assertNotMainThread();
    final SupportSQLiteDatabase _db = super.getOpenHelper().getWritableDatabase();
    try {
      super.beginTransaction();
      _db.execSQL("DELETE FROM `BINS`");
      _db.execSQL("DELETE FROM `BIN_ITEMS`");
      _db.execSQL("DELETE FROM `ITEMS`");
      _db.execSQL("DELETE FROM `BFO_ITEM`");
      _db.execSQL("DELETE FROM `BFO_PRICE_CHECK`");
      super.setTransactionSuccessful();
    } finally {
      super.endTransaction();
      _db.query("PRAGMA wal_checkpoint(FULL)").close();
      if (!_db.inTransaction()) {
        _db.execSQL("VACUUM");
      }
    }
  }

  @Override
  public GenericDao genericDao() {
    if (_genericDao != null) {
      return _genericDao;
    } else {
      synchronized(this) {
        if(_genericDao == null) {
          _genericDao = new GenericDao_Impl(this);
        }
        return _genericDao;
      }
    }
  }
}
