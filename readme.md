# Project Title

Paris Group item count tacking

## Getting Started

* compile and deploy to device
* create folder for prerequisite data ParidGroup under root/Downloads, with subfolder data and images
* images folder to hold item images with item barcode number as image file name as-in <barcode>.jpg 
* data folder has users.txt containing user_id,user_name,user_passwd
* data folder has itemMast.db containing items table
* bin / shipment doc no should be mimimum 5 characters

### Prerequisites

* ~/Downloads/ParisGroup/data/users.txt
* ~/Downloads/ParisGroup/data/itemMast.db
* ~/Downloads/ParisGroup/images/  

### Items table in items.db
CREATE TABLE ITEMS (
    BARCODE          TEXT,
    ITEM_NO          TEXT,
    PRODUCT_CODE     TEXT,
    BRAND            TEXT,
    ITEM_CODE        TEXT,
    ITEM_DESCRIPTION TEXT,
    IMAGEFILENAME    TEXT,
    PRIMARY KEY (
        BARCODE
    )
);